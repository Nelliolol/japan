# Japan / English learning helper

The project consist of two functions. 
* Japanese - Eng/Ru dictionary.
* [Anki](https://apps.ankiweb.net/) export file creator. 
  Anki is a program which makes remembering things easy.

## Project install
* Create env ```cp .env.example .env```
* Fill vars
* Run ```make init``` in the project root directory.
* Open http://localhost:BACKEND_PORT/

### Run project
```
make up
```

### Stop project
```
make down
```

### Run test
```
make test
```

## Generate Anki export file

The following commands will create export files
```project/storage/app/anki_export.txt```
```project/storage/app/anki_export_eng.txt```

Audio files will be stored in
```project/storage/app/audio```

### Example for Japanese
```
make generate-anki-japan-file command='test_tag 増やす 伏せる 臥せる --debug'
```
#### Output file
```
#separator:;
#tags:add_97
増やす; увеличивать, приумножать Ср. 殖やす; ふやす; 彼は貯金を増やした
伏せる; 1. класть лицом вниз, переворачивать, 2. прятать, скрывать закапывать, 3. накрывать что-л.; ふせる; 床に伏せろ
臥せる; ложиться слечь Ср. 伏せる; ふせる; 私は頭痛で臥せっていました
```

### Example for English
```
make generate-anki-english-file command="eng_add_59 'fade up' 'overstate' 'fuel' --audio --debug"
```
#### Output file
```
#separator:;
#tags:eng_add_59
misunderstanding; |ˌmɪsʌndərˈstændɪŋ|; неправильное понимание, недоразумение отсутствие взаимопонимания, размолвка; ; ; Maybe it's all just a big misunderstanding.; [sound:677724ee-51fa-4f66-88c5-512e72ae4816.mp3]
overstate; |ˌəʊvərˈsteɪt|; ; преувеличивать; ; The gravity of the situation cannot be overstated.; [sound:6014d411-b878-4a77-8839-3fb58c8e21b4.mp3]
fuel; |ˈfjuːəl|; топливо, горючее, разжигание страстей; заправлять горючим или топливом, запасаться топливом заправляться горючим, питать, поддерживать, заливать топливо питать топливом, принимать топливо, грузить топливо; ; We stopped to take on fuel.; [sound:60c4d683-2774-4234-bdef-0500dc2267e7.mp3]
```

[//]: # (### Run tests)

[//]: # (```)

[//]: # (make unit)

[//]: # (make e2e)

[//]: # (```)

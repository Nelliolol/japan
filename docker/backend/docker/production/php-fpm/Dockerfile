FROM php:8.1-cli-alpine as builder

RUN apk update && apk add unzip npm

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV NODE_OPTIONS --openssl-legacy-provider

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/bin --filename=composer --quiet \
    && rm -rf /root/.composer/cache

WORKDIR /app

COPY ./project/composer.json ./project/composer.lock ./
COPY ./project/package.json ./project/package-lock.json ./project/webpack.mix.js ./
COPY ./project/resources/ ./resources/
RUN npm install && npm run dev

COPY ./project/database ./database

RUN composer install --no-dev --prefer-dist --no-progress --no-suggest --no-scripts --optimize-autoloader \
    && rm -rf /root/.composer/cache

### FPM ###

FROM php:8.1-fpm-alpine

RUN docker-php-ext-install opcache

RUN mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini

COPY ./docker/backend/docker/common/php/conf.d /usr/local/etc/php/conf.d
COPY ./docker/backend/docker/production/php/conf.d /usr/local/etc/php/conf.d

WORKDIR /app

COPY --from=builder /app ./
COPY ./project/ ./
RUN chmod -R 777 storage

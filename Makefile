init: docker-down-clear docker-build composer-install build-laravel-mix docker-up
up: docker-up
down: docker-down
restart: down up
build: build-gateway build-backend
push: push-gateway push-backend
full-deploy: build push deploy

docker-up:
	docker compose up -d

docker-down:
	docker compose down --remove-orphans

docker-down-clear:
	docker compose down -v --remove-orphans

docker-build:
	docker compose build

cli-backend:
	docker compose run --rm backend-php-cli $(command)

generate-anki-english-file:
	docker compose run --rm backend-php-cli php artisan anki-file-generate:english $(command)

generate-anki-japan-file:
	docker compose run --rm backend-php-cli php artisan anki-file-generate:japan $(command)

test: test-backend-unit test-backend-feature test-english-word-parser

test-backend-unit:
	docker compose run --rm backend-php-cli-test-unit php vendor/bin/phpunit --testsuite Unit $(command)

test-backend-feature:
	docker compose run --rm backend-php-cli-test-feature php vendor/bin/phpunit --testsuite Feature $(command)

test-english-word-parser:
	docker compose run --rm english-word-parser go test -v ./tests

test-japanese-sentence-parser:
	docker compose run --rm japanese-sentence-parser go test -v ./tests

move-audio-files:
	cp -a project/storage/app/audio/. "/home/max/.local/share/Anki2/1-й пользователь/collection.media"

composer-install:
	docker compose run --rm backend-php-cli composer install

build-laravel-mix: npm-install npm-run-dev

npm-install:
	docker compose run --rm backend-php-cli npm install

npm-run-dev:
	docker compose run --rm backend-php-cli npm run dev

build-gateway:
	docker --log-level=debug build --pull --file=docker/gateway/docker/production/nginx/Dockerfile --tag=${REGISTRY}/japan-gateway:${IMAGE_TAG} docker/gateway/docker

build-backend:
	docker --log-level=debug build --pull --file=docker/backend/docker/production/php-cli/Dockerfile --tag=${REGISTRY}/japan-backend-php-cli:${IMAGE_TAG} .
	docker --log-level=debug build --pull --file=docker/backend/docker/production/php-fpm/Dockerfile --tag=${REGISTRY}/japan-backend-php-fpm:${IMAGE_TAG} .
	docker --log-level=debug build --pull --file=docker/backend/docker/production/nginx/Dockerfile --tag=${REGISTRY}/japan-backend:${IMAGE_TAG} .
	docker --log-level=debug build --pull --file=docker/go/production/english-word-parser/Dockerfile --tag=${REGISTRY}/japan-english-word-parser:${IMAGE_TAG} .
	docker --log-level=debug build --pull --file=docker/go/production/japanese-sentence-parser/Dockerfile --tag=${REGISTRY}/japan-japanese-sentence-parser:${IMAGE_TAG} .

try-build:
	REGISTRY=localhost IMAGE_TAG=0 make build

push-gateway:
	docker push ${REGISTRY}/japan-gateway:${IMAGE_TAG}

push-backend:
	docker push ${REGISTRY}/japan-backend-php-cli:${IMAGE_TAG}
	docker push ${REGISTRY}/japan-backend:${IMAGE_TAG}
	docker push ${REGISTRY}/japan-backend-php-fpm:${IMAGE_TAG}
	docker push ${REGISTRY}/japan-english-word-parser:${IMAGE_TAG}
	docker push ${REGISTRY}/japan-japanese-sentence-parser:${IMAGE_TAG}

deploy:
	ssh ${HOST} -p ${PORT} 'rm -rf site_${BUILD_NUMBER}'
	ssh ${HOST} -p ${PORT} 'mkdir site_${BUILD_NUMBER}'
	ssh ${HOST} -p ${PORT} 'mkdir -p db'
	scp -P ${PORT} docker-compose-production.yml ${HOST}:site_${BUILD_NUMBER}/docker-compose.yml
	scp -P ${PORT} .env ${HOST}:site_${BUILD_NUMBER}/.env
	ssh ${HOST} -p ${PORT} "cd site_${BUILD_NUMBER} && sed -i "s/APP_DEBUG=true/APP_DEBUG=false/" .env"
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && sed -i "s/DATA_PATH_HOST=\.\/docker\/db/DATA_PATH_HOST=\/root\/db/" .env'
	ssh ${HOST} -p ${PORT} "cd site_${BUILD_NUMBER} && sed -i "s/APP_ENV=local/APP_ENV=production/" .env"
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && sed -i "s/APP_URL=http:\/\/localhost:8092/APP_URL=https:\/\/japan.hackmycredit.ru/" .env'
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "COMPOSE_PROJECT_NAME=japan" >> .env'
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "REGISTRY=${REGISTRY}" >> .env'
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && echo "IMAGE_TAG=${IMAGE_TAG}" >> .env'
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker compose pull'
	ssh ${HOST} -p ${PORT} 'cd site_${BUILD_NUMBER} && docker compose up --build --remove-orphans -d'
	ssh ${HOST} -p ${PORT} 'rm -f site'
	ssh ${HOST} -p ${PORT} 'ln -sr site_${BUILD_NUMBER} site'

<?php

namespace App\Console\Commands;

use App\Services\Telegram\TelegramClient;
use Illuminate\Console\Command;

class SetTelegramWebHook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:webhook {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set telegram webHook.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param TelegramClient $telegram
     * @return int
     */
    public function handle(
        TelegramClient $telegram
    ): int
    {
        $secret_token = env('TELEGRAM_RESPONSE_TOKEN');
        $url = $this->argument('url');
        $telegram->setWebhook($url, $secret_token);
        dump( $telegram->getWebhookInfo() );
        return 0;
    }
}

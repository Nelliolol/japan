<?php

namespace App\Console\Commands;

use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Command as HandlerCommand;
use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Handler;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class GenerateAnkiExportFileJapanese extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anki-file-generate:japanese {tag} {phrase*} {--debug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Anki export file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(
        Handler $handler
    )
    {
        $tag = $this->argument('tag');
        $ar_words = $this->argument('phrase');
        $path_to_export_file = Storage::path('anki_export.txt');

        $command = new HandlerCommand(
            $tag,
            $ar_words,
            $path_to_export_file,
        );

        $response = $handler->handle($command);

        if ( $this->option('debug') ){

            dump($response->getParsedWordCard());

            $this->line(sprintf('Words to export: %d', $response->countWordsToExport()));
            $this->line(sprintf('Parsed words: %d', $response->countParsedWords()));
            $this->line(sprintf('Not parsed words: %d', $response->countNotParsedWords()));

            if ( $response->getNotFoundWords() ){
                dump($response->getNotFoundWords());
            }
        }
    }
}

<?php

namespace App\Console\Commands;

use App\Services\Telegram\TelegramClient;
use Illuminate\Console\Command;

class TelegramSendMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:send {chat_id} {message}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send telegram message.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param TelegramClient $telegram
     * @return int
     */
    public function handle(
        TelegramClient $telegram
    ): int
    {
        $message = $this->argument('message');
        $chat_id = $this->argument('chat_id');
        $telegram->sendMessage($chat_id, $message);
        return 0;
    }
}

<?php

namespace App\Console\Commands;

use App\UseCases\English\Commands\GenerateAnkiExportFile\Command as HandlerCommand;
use App\UseCases\English\Commands\GenerateAnkiExportFile\Handler;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class GenerateAnkiExportFileEnglish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anki-file-generate:english {tag} {phrase*} {--audio} {--debug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Anki export file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param Handler $handler
     * @return int
     */
    public function handle(
        Handler $handler
    ): int
    {
        $tag = $this->argument('tag');
        $ar_words = $this->argument('phrase');
        $path_to_export_file = Storage::path('anki_export_eng.txt');
        $path_to_audio_directory = Storage::path('audio/');

        $command = new HandlerCommand(
            $tag,
            $ar_words,
            $path_to_export_file,
            $path_to_audio_directory
        );

        if ($this->option('audio')) {
            $command->enableDownloadingAudioFiles();
        } else {
            $command->disableDownloadingAudioFiles();
        }

        $response = $handler->handle($command);

        if ($this->option('debug')) {
            dump($response->getParsedWordCard());

            $this->line(sprintf('Words to export: %d', $response->countWordsToExport()));
            $this->line(sprintf('Parsed words: %d', $response->countParsedWords()));
            $this->line(sprintf('Not parsed words: %d', $response->countNotParsedWords()));

            if ($response->getNotFoundWords()) {
                dump($response->getNotFoundWords());
            }
        }

        return 0;
    }
}

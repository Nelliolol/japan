<?php

namespace App\Http\Controllers;

use App\Services\Telegram\TelegramClient;
use App\Services\Telegram\TelegramHelper;
use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Response;
use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Command as HandlerCommand;
use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Handler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ApiController extends Controller
{
    public function incomingMessage(
        Request        $request,
        Handler        $anki_file_generate_handler,
        TelegramClient $telegram
    )
    {
        if (!$this->isRequestFromTelegram($request)) {
            return;
        }
        if (!$ar_words_to_export = $this->getWordsToExport($request)) {
            return;
        }
        if (!$chat_id = $this->getChatID($request)) {
            return;
        }
        if (!$message_id = $this->getMessageID($request)) {
            return;
        }

        $export_file_name = (string) Str::uuid() . '.txt';
        $path_to_export_file = Storage::path($export_file_name);

        $command = new HandlerCommand(
            'telegram',
            $ar_words_to_export,
            $path_to_export_file,
        );

        try {
            $response = $anki_file_generate_handler->handle($command);

            $message = $this->createTelegramResponseMessage($response);
            $telegram->sendMessage($chat_id, $message, $message_id);

            if ($response->countParsedWords() > 0) {
                $telegram->sendDocument($chat_id, $path_to_export_file, $message_id);
            }

        } catch ( \DomainException $e ){
            $telegram->sendMessage($chat_id, $e->getMessage(), $message_id);
        } catch ( \Throwable $e ){
            Log::error("Telegram send message error. " . $e->getMessage());
        } finally {
            Storage::delete($export_file_name);
        }
    }

    private function isRequestFromTelegram(Request $request): bool
    {
        return $request->header(TelegramHelper::TELEGRAM_HEADER) == env('TELEGRAM_RESPONSE_TOKEN');
    }

    private function getWordsToExport(Request $request): array
    {
        if (empty($request->all()['message']['text'])) {
            return [];
        }

        $message = $request->all()['message']['text'];
        $command = TelegramHelper::getCommandPrefix();

        if (!str_contains($message, $command)) {
            return [];
        }

        $message = str_replace($command, '', $message);
        $message = trim($message);

        if (strlen($message) < 1) {
            return [];
        }

        return explode(' ', $message);
    }

    private function getChatID(Request $request): ?int
    {
        if (empty($request->all()['message']['chat']['id'])) {
            return null;
        }
        return $request->all()['message']['chat']['id'];
    }

    private function getMessageID(Request $request): ?int
    {
        if (empty($request->all()['message']['message_id'])) {
            return null;
        }
        return $request->all()['message']['message_id'];
    }

    private function createTelegramResponseMessage(Response $response): string
    {
        $message = sprintf('Words to export: %d', $response->countWordsToExport());
        $message .= "\n" . sprintf('Parsed words: %d', $response->countParsedWords());
        $message .= "\n" . sprintf('Not parsed words: %d', $response->countNotParsedWords());
        foreach ($response->getNotFoundWords() as $word) {
            $message .= "\n" . sprintf('%s : was not found', $word);
        }
        return $message;
    }
}

<?php

namespace App\Http\Controllers;

use App\UseCases\Japanese\Queries\GetKanjiInformation\Command;
use App\UseCases\Japanese\Queries\GetKanjiInformation\Handler;

class HomeController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function find(
        string  $search_kanji,
        Handler $handler
    )
    {
        $result = $handler->handle(new Command($search_kanji));

        return view('find', compact(
            'result',
            'search_kanji',
        ));
    }
}

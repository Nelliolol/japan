<?php

namespace App\Providers;

use App\Services\Telegram\TelegramBotClient;
use App\Services\Telegram\TelegramClient;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
class TelegramClientProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(TelegramClient::class, function (Application $app) {
            return new TelegramBotClient(env("TELEGRAM_TOKEN"));
        });
    }
}

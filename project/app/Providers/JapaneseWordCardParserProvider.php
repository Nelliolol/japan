<?php

namespace App\Providers;

use App\UseCases\Japanese\Services\KanjiParser\Web\Jisho\Jisho;
use App\UseCases\Japanese\Services\KanjiParser\Web\Yarxi\Yarxi;
use App\UseCases\Japanese\Services\SentenceParser\Parser as JapaneseSentenceParser;
use App\UseCases\Japanese\Services\WordCardParser\Parser;
use App\UseCases\Japanese\Services\WordCardParser\YarxiAndJishoParser\YarxiAndJishoParser;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class JapaneseWordCardParserProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(Parser::class, function (Application $app) {
            return new YarxiAndJishoParser(
                $this->app->make(Yarxi::class),
                $this->app->make(Jisho::class),
                $this->app->make(JapaneseSentenceParser::class),
            );
        });
    }
}

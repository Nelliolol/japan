<?php

namespace App\Providers;

use App\UseCases\Japanese\Queries\GetKanjiInformation\Handler;
use App\UseCases\Japanese\Services\KanjiParser\Web\Jisho\ContentFinder\AddKanji;
use App\UseCases\Japanese\Services\KanjiParser\Web\Jisho\ContentFinder\MainKanji;
use App\UseCases\Japanese\Services\KanjiParser\Web\Jisho\Jisho;
use App\UseCases\Japanese\Services\KanjiParser\Web\Yarxi\ContentFinder\KanjiPage;
use App\UseCases\Japanese\Services\KanjiParser\Web\Yarxi\ContentFinder\WordPage;
use App\UseCases\Japanese\Services\KanjiParser\Web\Yarxi\Yarxi;
use App\UseCases\Japanese\Services\SentenceParser\Parser as JapaneseSentenceParser;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use React\Http\Browser;

class GetKanjiInformationProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(Yarxi::class, function (Application $app) {

            $yarxi = new Yarxi(new Browser());
            $yarxi->addContentFinder( new WordPage );
            $yarxi->addContentFinder( new KanjiPage );

            return $yarxi;
        });

        $this->app->singleton(Jisho::class, function (Application $app) {

            $jisho = new Jisho(new Browser());
            $jisho->addContentFinder( new AddKanji );
            $jisho->addContentFinder( new MainKanji );

            return $jisho;
        });

        $this->app->singleton(Handler::class, function (Application $app) {

            $ar_kanji_parsers = [];
            $ar_kanji_parsers[] = $this->app->make(Yarxi::class);
            $ar_kanji_parsers[] = $this->app->make(Jisho::class);

           return new Handler(
               $ar_kanji_parsers,
               $this->app->make(JapaneseSentenceParser::class)
           );
        });
    }
}

<?php

namespace App\Providers;

use App\Services\FileHelper\AsyncDownloadFileHelper;
use App\Services\FileHelper\FileHelper;
use App\Services\HttpClient\GuzzleAdapter;
use App\Services\HttpClient\HttpClient;
use App\Services\UUIDGenerator\LaravelStringUUIDGenerator;
use App\Services\UUIDGenerator\UUIDGenerator;
use App\UseCases\English\Services\Export\EnglishCardExporter;
use App\UseCases\English\Services\Export\Exporter as EnglishExporter;
use App\UseCases\English\Services\Parser\GoMicroserviceParser;
use App\UseCases\English\Services\Parser\Parser as EnglishParser;
use App\UseCases\Japanese\Services\Export\Exporter as JapaneseExporter;
use App\UseCases\Japanese\Services\Export\JapaneseCardExporter;
use App\UseCases\Japanese\Services\SentenceParser\JishoMicroserviceParser;
use App\UseCases\Japanese\Services\SentenceParser\Parser as JapaneseSentenceParser;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        EnglishParser::class => GoMicroserviceParser::class,
        EnglishExporter::class => EnglishCardExporter::class,
        FileHelper::class => AsyncDownloadFileHelper::class,
        UUIDGenerator::class => LaravelStringUUIDGenerator::class,
        HttpClient::class => GuzzleAdapter::class,
        JapaneseExporter::class => JapaneseCardExporter::class,
        JapaneseSentenceParser::class => JishoMicroserviceParser::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

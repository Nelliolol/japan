<?php

function smartTrim( $str ){
    $str = trim($str);
    if ( substr($str, 0, 1) == '-' ){
        $str = substr($str, 1);
    }

    $str = htmlspecialchars_decode($str, ENT_QUOTES);
    $str = html_entity_decode($str);
    $str = strip_tags($str);
    $str = str_replace(";", "", $str);
    $str = preg_replace("/\r|\n/", "", $str);
    $str = preg_replace("/\\s+/iu"," ",$str);
    $str = trim($str);
    return $str;
}

<?php

namespace App\Services\HttpClient;

use Psr\Http\Message\ResponseInterface;

interface HttpClient
{
    public function get(string $uri): ResponseInterface;
    public function post(string $uri, array $options = []): ResponseInterface;
}

<?php

namespace App\Services\HttpClient;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class GuzzleAdapter implements HttpClient
{
    private Client $http_client;

    public function __construct()
    {
        $this->http_client = new Client([
            // 'timeout'  => 5.0,
            'verify' => false,
        ]);
    }

    public function get(string $uri, array $options = []): ResponseInterface
    {
        return $this->http_client->get($uri, $options);
    }

    public function post(string $uri, array $options = []): ResponseInterface
    {
        return $this->http_client->post($uri, $options);
    }
}

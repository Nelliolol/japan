<?php

namespace App\Services\UUIDGenerator;

interface UUIDGenerator
{
    public function generate(): string;
}

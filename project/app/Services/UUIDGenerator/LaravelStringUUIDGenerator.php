<?php

namespace App\Services\UUIDGenerator;

use Illuminate\Support\Str;

class LaravelStringUUIDGenerator implements UUIDGenerator
{
    public function generate(): string
    {
        return Str::uuid();
    }
}

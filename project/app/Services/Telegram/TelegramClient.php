<?php

namespace App\Services\Telegram;

interface TelegramClient
{
    public function sendMessage(int $chat_id, string $message, ?int $message_id = null);

    public function sendDocument(int $chat_id, string $path_to_file, ?int $message_id = null);

    public function setWebhook(string $url, string $secret_token);

    public function getWebhookInfo(): mixed;
}

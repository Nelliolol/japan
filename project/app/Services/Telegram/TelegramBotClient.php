<?php

namespace App\Services\Telegram;

use CURLFile;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Exception;
use TelegramBot\Api\InvalidArgumentException;

class TelegramBotClient implements TelegramClient
{
    private BotApi $bot;

    /**
     * @throws \Exception
     */
    public function __construct(string $token)
    {
        $this->bot = new BotApi($token);
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function sendMessage(int $chat_id, string $message, ?int $message_id = null)
    {
        $this->bot->sendMessage($chat_id, $message, replyToMessageId: $message_id);
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function sendDocument(int $chat_id, string $path_to_file, ?int $message_id = null)
    {
        $document = new CURLFile($path_to_file);
        $this->bot->sendDocument($chat_id, $document, replyToMessageId: $message_id);
    }

    /**
     * @throws Exception
     */
    public function setWebhook(string $url, string $secret_token)
    {
        $this->bot->setWebhook($url, secret_token: $secret_token);
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function getWebhookInfo(): mixed
    {
        return $this->bot->getWebhookInfo();
    }
}

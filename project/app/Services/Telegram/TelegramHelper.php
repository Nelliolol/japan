<?php

namespace App\Services\Telegram;

class TelegramHelper
{
    const TELEGRAM_HEADER = 'X-Telegram-Bot-Api-Secret-Token';

    const TELEGRAM_COMMAND = 'generate';

    public static function getCommandPrefix(): string
    {
        return '/' . TelegramHelper::TELEGRAM_COMMAND . ' ';
    }
}

<?php

namespace App\Services\FileHelper\DTO;

use Webmozart\Assert\Assert;

class DownloadTask
{
    public string $url;
    public string $path_to_save_file;

    /**
     * @param string $url
     * @param string $path_to_save_file
     */
    public function __construct(string $url, string $path_to_save_file)
    {
        Assert::notEmpty($url);
        $this->url = $url;
        Assert::notEmpty($path_to_save_file);
        $this->path_to_save_file = $path_to_save_file;
    }
}

<?php

namespace App\Services\FileHelper;

use App\Services\FileHelper\DTO\DownloadTask;

interface FileHelper
{
    public function put(string $path_to_file, string $content): void;
    public function deleteDirectory(string $path_to_directory): void;

    /**
     * @param DownloadTask[] $ar_tasks
     * @return void
     */
    public function downloadFiles(array $ar_tasks): void;
    public function exists(string $path_to_file): bool;
}

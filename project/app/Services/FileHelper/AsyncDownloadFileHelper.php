<?php

namespace App\Services\FileHelper;

use App\Services\FileHelper\DTO\DownloadTask;
use Psr\Http\Message\ResponseInterface;
use React\Filesystem\AdapterInterface;
use React\Http\Browser;
use React\Filesystem\Factory;
use React\Promise\Deferred;
use React\Promise\PromiseInterface;
use Throwable;
use function React\Async\await;
use function React\Promise\all;

class AsyncDownloadFileHelper implements FileHelper
{
    private AdapterInterface $filesystem;
    private Browser $browser;

    public function __construct()
    {
        $this->filesystem = Factory::create();
        $this->browser = new Browser();
    }

    public function put(string $path_to_file, string $content): void
    {
        $this->createDirectories($path_to_file);
        file_put_contents($path_to_file, $content);
    }

    public function deleteDirectory(string $path_to_directory): void
    {
        if (is_dir($path_to_directory)) {
            array_map('unlink', glob("$path_to_directory/*.*"));
            rmdir($path_to_directory);
        }
    }

    /**
     * @param DownloadTask[] $ar_tasks
     * @return void
     * @throws Throwable
     */
    public function downloadFiles(array $ar_tasks): void
    {
        $promises = [];
        foreach ($ar_tasks as $task) {
            $this->createDirectories($task->path_to_save_file);
            $promises[] = $this->createDownloadPromise($task->url, $task->path_to_save_file);
        }

        await(all($promises));
    }

    /**
     * @param string $path
     * @return void
     */
    public function createDirectories(string $path): void
    {
        $dir_path = pathinfo($path, PATHINFO_DIRNAME);
        if (!is_dir($dir_path)) {
            mkdir($dir_path, 0777, true);
        }
    }

    private function createDownloadPromise($url, $path): PromiseInterface
    {
        return $this->browser->requestStreaming(
            'GET',
            $url,
        )->then(function (ResponseInterface $response) use ($path) {

            $deferred = new Deferred();
            $body = $response->getBody();

            $body->on('data', function ($chunk) use ($path) {
                $this->filesystem->file($path)->putContents($chunk, FILE_APPEND);
            });

            $body->on('close', function () use ($deferred) {
                $deferred->resolve();
            });

            return $deferred->promise();
        }, fn() => null);
    }

    public function exists(string $path_to_file): bool
    {
        return file_exists($path_to_file);
    }
}


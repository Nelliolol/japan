<?php

namespace App\Services\FileHelper;

use App\Services\FileHelper\DTO\DownloadTask;
use Exception;
use Illuminate\Support\Facades\Storage;

class LaravelStorageFileHelper implements FileHelper
{
    public function put(string $path_to_file, string $content): void
    {
        $result = storage::put($this->toRelativePath($path_to_file), $content);
        if ($result === false) {
            throw new \DomainException('Put file error');
        }
    }

    private function toRelativePath( string $absolute_path ): string
    {
        return str_replace(storage_path('app'), '', $absolute_path);
    }

    public function deleteDirectory(string $path_to_directory): void
    {
        $result = Storage::deleteDirectory($this->toRelativePath($path_to_directory));
        if ($result === false) {
            throw new \DomainException('deleteDirectory error');
        }
    }

    /**
     * @param DownloadTask[] $ar_tasks
     * @return void
     */
    public function downloadFiles(array $ar_tasks): void
    {
        foreach ($ar_tasks as $task) {
            try {
                $this->downloadFile($task);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    /**
     * @param DownloadTask $task
     * @return void
     */
    private function downloadFile(DownloadTask $task): void
    {
        $this->put($task->path_to_save_file, file_get_contents($task->url));
    }

    public function exists(string $path_to_file): bool
    {
        return storage::exists($this->toRelativePath($path_to_file));
    }
}

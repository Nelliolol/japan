<?php

namespace App\UseCases\Japanese\Commands\GenerateAnkiExportFile;

use App\Services\FileHelper\FileHelper;
use App\UseCases\Japanese\Entities\WordCard;
use App\UseCases\Japanese\Services\Export\Exporter;
use App\UseCases\Japanese\Services\WordCardParser\Parser;
use DomainException;

class Handler
{
    private Parser $word_parser;
    private Exporter $exporter;
    private FileHelper $file_helper;

    /**
     * @param Parser $word_parser
     * @param Exporter $exporter
     * @param FileHelper $file_helper
     */
    public function __construct(Parser $word_parser, Exporter $exporter, FileHelper $file_helper)
    {
        $this->word_parser = $word_parser;
        $this->exporter = $exporter;
        $this->file_helper = $file_helper;
    }

    public function handle(Command $command): Response
    {
        $ar_word_cards = $this->getWordsToExport($command->getArWordsToExport());
        $this->generateExportFile($ar_word_cards, $command->getTag(), $command->getPathToExportFile());
        return $this->makeResponse($command->getArWordsToExport(), $ar_word_cards);
    }

    /**
     * @param array $ar_parsed_words
     * @return WordCard[]
     */
    private function getWordsToExport(array $ar_parsed_words): array
    {
        $ar_parsed_words = $this->word_parser->parseMany($ar_parsed_words);
        if (empty($ar_parsed_words)) {
            throw new DomainException("No data for export");
        }
        return $ar_parsed_words;
    }

    /**
     * @param WordCard[] $ar_word_cards
     * @param string $tag
     * @param string $path_to_export_file
     * @return void
     */
    private function generateExportFile(array $ar_word_cards, string $tag, string $path_to_export_file): void
    {
        $export_text = $this->exporter->renderExportFile($ar_word_cards, $tag);
        $this->file_helper->put($path_to_export_file, $export_text);
    }

    /**
     * @param string[] $ar_words_to_export
     * @param WordCard[] $parsed_word_card
     */
    private function makeResponse( array $ar_words_to_export, array $parsed_word_card ): Response
    {
        return new Response($ar_words_to_export, $parsed_word_card);
    }
}

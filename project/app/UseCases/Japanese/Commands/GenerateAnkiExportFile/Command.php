<?php

namespace App\UseCases\Japanese\Commands\GenerateAnkiExportFile;

use Webmozart\Assert\Assert;

class Command
{
    /**
     * @var string[]
     */
    private array $ar_words_to_export;
    private string $tag;
    private string $path_to_export_file;
    private bool $debug = false;

    /**
     * @param string $tag
     * @param string[] $ar_words_to_export
     * @param string $path_to_export_file
     */
    public function __construct(
        string $tag,
        array $ar_words_to_export,
        string $path_to_export_file
    )
    {
        Assert::notEmpty($tag);
        $this->tag = $tag;

        Assert::notEmpty($ar_words_to_export);
        $this->ar_words_to_export = $ar_words_to_export;

        Assert::notEmpty($tag);
        $this->path_to_export_file = $path_to_export_file;
    }

    /**
     * @return array
     */
    public function getArWordsToExport(): array
    {
        return $this->ar_words_to_export;
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @return string
     */
    public function getPathToExportFile(): string
    {
        return $this->path_to_export_file;
    }
}

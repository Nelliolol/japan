<?php

namespace App\UseCases\Japanese\Commands\GenerateAnkiExportFile;

use App\UseCases\Japanese\Entities\WordCard;
use Webmozart\Assert\Assert;

class Response
{
    /**
     * @var WordCard[]
     */
    private array $parsed_word_card;
    /**
     * @var string[] $ar_words_to_export
     */
    private array $ar_words_to_export;

    /**
     * @param WordCard[] $parsed_word_card
     * @param string[] $ar_words_to_export
     */
    public function __construct(array $ar_words_to_export, array $parsed_word_card)
    {
        Assert::notEmpty($ar_words_to_export);
        $this->ar_words_to_export = $ar_words_to_export;
        $this->parsed_word_card = $parsed_word_card;
    }

    public function countWordsToExport(): int
    {
        return count($this->ar_words_to_export);
    }

    public function countParsedWords(): int
    {
        return count($this->parsed_word_card);
    }

    public function countNotParsedWords(): int
    {
        return $this->countWordsToExport() - $this->countParsedWords();
    }

    /**
     * @return WordCard[]
     */
    public function getParsedWordCard(): array
    {
        return $this->parsed_word_card;
    }

    /**
     * @return string[]
     */
    public function getNotFoundWords(): array
    {
        $ar_not_found_words = [];
        foreach ( $this->ar_words_to_export as $word ){
            if ( !$this->wasWordCardParsed($word) ){
                $ar_not_found_words[] = $word;
            }
        }

        return $ar_not_found_words;
    }

    /**
     * @param string $word
     * @return bool
     */
    private function wasWordCardParsed(string $word): bool
    {
        foreach ($this->parsed_word_card as $word_card) {
            if ($word_card->getKanji() == $word) {
                return true;
            }
        }
        return false;
    }

}

<?php

namespace App\UseCases\Japanese\Queries\GetKanjiInformation;

use App\UseCases\Japanese\Entities\KanjiParseResult;
use App\UseCases\Japanese\Services\KanjiParser\Parser as KanjiParser;
use App\UseCases\Japanese\Services\SentenceParser\Parser as SentenceParser;

class Handler
{
    /**
     * @var KanjiParser[]
     */
    private array $ar_kanji_parsers;
    private SentenceParser $sentence_parser;

    /**
     * @param array $ar_kanji_parsers
     * @param SentenceParser $sentence_parser
     */
    public function __construct( array $ar_kanji_parsers, SentenceParser $sentence_parser)
    {
        $this->ar_kanji_parsers = $ar_kanji_parsers;
        $this->sentence_parser = $sentence_parser;
    }

    public function handle(Command $command): Response
    {
        return new Response(
            $this->sentence_parser->parse($command->getSearchKanji()),
            $this->getKanjiParsedResult($command->getSearchKanji())
        );
    }

    /**
     * @param string $search_kanji
     * @return KanjiParseResult[]
     */
    private function getKanjiParsedResult(string $search_kanji): array
    {
        $kanji_parse_result = [];
        foreach ($this->ar_kanji_parsers as $parser) {
            $kanji_parse_result[] = $parser->parse($search_kanji);
        }
        return $kanji_parse_result;
    }
}

<?php

namespace App\UseCases\Japanese\Queries\GetKanjiInformation;

use App\UseCases\Japanese\Entities\KanjiParseResult;
use App\UseCases\Japanese\Entities\SentenceBox;

class Response
{
    private SentenceBox $parsed_sentences;
    /**
     * @var KanjiParseResult[]
     */
    private array $ar_kanji_parse_results;

    /**
     * @param SentenceBox $parsed_sentences
     * @param KanjiParseResult[] $ar_kanji_parse_results
     */
    public function __construct(SentenceBox $parsed_sentences, array $ar_kanji_parse_results)
    {
        $this->parsed_sentences = $parsed_sentences;
        $this->ar_kanji_parse_results = $ar_kanji_parse_results;
    }

    /**
     * @return SentenceBox
     */
    public function getParsedSentences(): SentenceBox
    {
        return $this->parsed_sentences;
    }

    /**
     * @return KanjiParseResult[]
     */
    public function getArKanjiParseResults(): array
    {
        return $this->ar_kanji_parse_results;
    }
}

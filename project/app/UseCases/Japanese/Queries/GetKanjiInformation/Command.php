<?php

namespace App\UseCases\Japanese\Queries\GetKanjiInformation;

use Webmozart\Assert\Assert;

class Command
{
    private string $search_kanji;

    /**
     * @param string $search_kanji
     */
    public function __construct(string $search_kanji)
    {
        Assert::notEmpty($search_kanji);
        $this->search_kanji = $search_kanji;
    }

    /**
     * @return string
     */
    public function getSearchKanji(): string
    {
        return $this->search_kanji;
    }
}

<?php

namespace App\UseCases\Japanese\Services\WordCardParser\YarxiAndJishoParser;

use App\UseCases\Japanese\Entities\Kanji;
use App\UseCases\Japanese\Entities\KanjiParseResult;
use App\UseCases\Japanese\Entities\SentenceBox;
use App\UseCases\Japanese\Entities\WordCard;

class WordCardFabric
{
    private string $word;
    private ?Kanji $jisho_kanji;
    private ?Kanji $yarxi_kanji;
    private ?SentenceBox $sentence_box;

    /**
     * @param string $word
     * @param KanjiParseResult|null $yarxi_kanji_parse_result
     * @param KanjiParseResult|null $jisho_kanji_parse_result
     * @param SentenceBox|null $sentence_box
     */
    public function __construct(
        string            $word,
        ?KanjiParseResult $yarxi_kanji_parse_result,
        ?KanjiParseResult $jisho_kanji_parse_result,
        ?SentenceBox      $sentence_box
    )
    {
        $this->word = $word;
        $this->sentence_box = $sentence_box;
        $this->jisho_kanji = $this->findKanjiWithExactlySameName($jisho_kanji_parse_result);
        $this->yarxi_kanji = $this->findKanjiWithExactlySameName($yarxi_kanji_parse_result);
    }

    private function findKanjiWithExactlySameName(?KanjiParseResult $kanji_parse_result): ?Kanji
    {
        if (is_null($kanji_parse_result)) {
            return null;
        }

        foreach ($kanji_parse_result->getKanji() as $kanji) {
            if ($kanji->getName() == $this->word) {
                return $kanji;
            }
        }

        return null;
    }

    public function createWordCard(): ?WordCard
    {
        if (!$this->findKanji()) {
            return null;
        }

        $word_card = new WordCard($this->findKanji());

        if ($reading = $this->findReading()) {
            $word_card->setReading($reading);
        }

        if ($translate = $this->findTranslate()) {
            $word_card->setTranslate($translate);
        }

        if ($example = $this->findExample()) {
            $word_card->setExample($example);
        }

        return $word_card;
    }

    private function findExample(): ?string
    {
        if (empty($this->sentence_box)) {
            return null;
        }

        $cur_length = null;
        $shortest_sentence = null;
        foreach ($this->sentence_box->getSentence() as $sentence) {
            $sentence_length = mb_strlen($sentence->getJapText());
            if (!$cur_length || $sentence_length < $cur_length) {
                $cur_length = $sentence_length;
                $shortest_sentence = $sentence;
            }
        }

        if ($shortest_sentence) {
            return $shortest_sentence->getJapText();
        }

        return null;
    }

    public function findKanji(): ?string
    {
        if ($this->jisho_kanji) {
            return $this->jisho_kanji->getName();
        }

        if ($this->yarxi_kanji) {
            return $this->yarxi_kanji->getName();
        }

        return null;
    }

    private function findReading(): ?string
    {
        if ($this->jisho_kanji && $this->jisho_kanji->getKun()) {
            return implode(', ', $this->jisho_kanji->getKun());
        }

        if ($this->yarxi_kanji && $this->yarxi_kanji->getReading()) {
            return $this->yarxi_kanji->getReading();
        }

        return null;
    }

    private function findTranslate(): ?string
    {
        if ($this->yarxi_kanji && $this->yarxi_kanji->getTranslate()) {
            return implode(', ', $this->yarxi_kanji->getTranslate());
        }

        if ($this->jisho_kanji && $this->jisho_kanji->getTranslate()) {
            return implode(', ', $this->jisho_kanji->getTranslate());
        }

        return null;
    }
}

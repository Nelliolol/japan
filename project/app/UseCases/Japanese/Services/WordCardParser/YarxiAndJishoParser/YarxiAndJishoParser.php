<?php

namespace App\UseCases\Japanese\Services\WordCardParser\YarxiAndJishoParser;

use App\UseCases\Japanese\Entities\KanjiParseResult;
use App\UseCases\Japanese\Entities\SentenceBox;
use App\UseCases\Japanese\Entities\WordCard;
use App\UseCases\Japanese\Services\KanjiParser\Parser as KanjiParser;
use App\UseCases\Japanese\Services\SentenceParser\Parser as SentenceParser;
use App\UseCases\Japanese\Services\WordCardParser\Parser;
use function React\Async\await;
use function React\Promise\all;

class YarxiAndJishoParser implements Parser
{
    private KanjiParser $yarxi_parser;
    private KanjiParser $jisho_parser;
    private SentenceParser $sentence_parser;

    /**
     * @param KanjiParser $yarxi_parser
     * @param KanjiParser $jisho_parser
     * @param SentenceParser $sentence_parser
     */
    public function __construct(KanjiParser $yarxi_parser, KanjiParser $jisho_parser, SentenceParser $sentence_parser)
    {
        $this->yarxi_parser = $yarxi_parser;
        $this->jisho_parser = $jisho_parser;
        $this->sentence_parser = $sentence_parser;
    }

    /**
     * @param string[] $ar_words
     * @return WordCard[]
     */
    public function parseMany(array $ar_words): array
    {
        $result = [];

        list(
            $yarxi_kanji_parse_result,
            $jisho_kanji_parse_result,
            $parsed_sentences,
        ) = $this->getKanjiAndSentences($ar_words);

        foreach ($ar_words as $word) {

            $word_card_fabric = new WordCardFabric(
                $word,
                !empty($yarxi_kanji_parse_result[$word]) ? $yarxi_kanji_parse_result[$word] : null,
                !empty($jisho_kanji_parse_result[$word]) ? $jisho_kanji_parse_result[$word] : null,
                !empty($parsed_sentences[$word]) ? $parsed_sentences[$word] : null
            );

            if ($word_card = $word_card_fabric->createWordCard()) {
                $result[] = $word_card;
            }
        }

        return $result;
    }

    /**
     * @param array $ar_words
     * @return array
     */
    public function getKanjiAndSentences(array $ar_words): array
    {
        $promises = array(
            \React\Async\async(function () use ($ar_words) {
                return $this->parseYarxiKanji($ar_words);
            })(),
            \React\Async\async(function () use ($ar_words) {
                return $this->parseJishoKanji($ar_words);
            })(),
            \React\Async\async(function () use ($ar_words) {
                return $this->parseSentences($ar_words);
            })(),
        );
        return await(all($promises));
    }

    /**
     * @param string[] $ar_words
     * @return KanjiParseResult[]
     */
    private function parseYarxiKanji(array $ar_words): array
    {
        $result = [];
        foreach ( $this->yarxi_parser->parseMany($ar_words) as $kanji_parse_result ){
            $result[$kanji_parse_result->getStringKanji()] = $kanji_parse_result;
        }

        return $result;
    }

    /**
     * @param string[] $ar_words
     * @return KanjiParseResult[]
     */
    private function parseJishoKanji(array $ar_words): array
    {
        $result = [];
        foreach ( $this->jisho_parser->parseMany($ar_words) as $kanji_parse_result ){
            $result[$kanji_parse_result->getStringKanji()] = $kanji_parse_result;
        }

        return $result;
    }

    /**
     * @param string[] $ar_words
     * @return SentenceBox[]
     */
    private function parseSentences(array $ar_words): array
    {
        $result = [];
        foreach ( $this->sentence_parser->parseMany($ar_words) as $sentenceBox ){
            $result[$sentenceBox->getKanji()] = $sentenceBox;
        }
        return $result;
    }
}

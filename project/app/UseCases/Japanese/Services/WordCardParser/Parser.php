<?php

namespace App\UseCases\Japanese\Services\WordCardParser;

use App\UseCases\Japanese\Entities\WordCard;

interface Parser
{
    /**
     * @param string[] $ar_words
     * @return WordCard[]
     */
    public function parseMany(array $ar_words): array;
}

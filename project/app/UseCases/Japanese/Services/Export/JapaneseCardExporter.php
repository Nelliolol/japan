<?php

namespace App\UseCases\Japanese\Services\Export;

use App\UseCases\Japanese\Entities\WordCard;

class JapaneseCardExporter implements Exporter
{
    /**
     * @param WordCard[] $arExportData
     * @param string $tag
     * @return string
     */
    public function renderExportFile(array $arExportData, string $tag): string
    {
        $separator = ';';
        return view('exporters.anki', compact('arExportData', 'tag', 'separator'))->render();
    }
}

<?php

namespace App\UseCases\Japanese\Services\Export;

use App\UseCases\Japanese\Entities\WordCard;

interface Exporter
{
    /**
     * @param WordCard[] $arExportData
     * @param string $tag
     * @return string
     */
    public function renderExportFile(array $arExportData, string $tag ):string;
}

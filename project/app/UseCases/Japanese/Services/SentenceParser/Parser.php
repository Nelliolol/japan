<?php

namespace App\UseCases\Japanese\Services\SentenceParser;

use App\UseCases\Japanese\Entities\SentenceBox;

interface Parser
{
    public function parse(string $kanji): SentenceBox;

    /**
     * @param string[] $ar_kanji
     * @return SentenceBox[]
     */
    public function parseMany(array $ar_kanji): array;
}

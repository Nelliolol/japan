<?php

namespace App\UseCases\Japanese\Services\SentenceParser;
use App\UseCases\Japanese\Entities\Sentence;
use App\UseCases\Japanese\Entities\SentenceBox;
use DomainException;
use Exception;
use React\Http\Browser;
use function React\Async\await;

class JishoMicroserviceParser implements Parser
{
    private Browser $browser;

    public function __construct( Browser $browser )
    {
        $this->browser = $browser;
    }

    public function parse(string $kanji): SentenceBox
    {
        try {

            $response = $this->getServiceResponse([$kanji]);
            $response_sentence = $response[0];
            if ( !empty($response_sentence['error']) ){
                throw new DomainException($response_sentence['error']);
            }
            if ( empty($response_sentence['sentences']) ){
                throw new DomainException("empty sentences");
            }

            return $this->createSentenceBox($kanji, $response_sentence['sentences']);

        } catch (Exception $e) {}

        return new SentenceBox($kanji);
    }

    /**
     * @param string[] $ar_kanji
     * @return SentenceBox[]
     */
    public function parseMany(array $ar_kanji): array
    {
        $result = [];
        $response = $this->getServiceResponse($ar_kanji);
        foreach ( $response as $response_sentence ) {
            if ( !empty($response_sentence['error']) || empty($response_sentence['sentences']) ){
                continue;
            }
            $result[] = $this->createSentenceBox($response_sentence['kanji'], $response_sentence['sentences']);
        }
        return $result;
    }

    private function getServiceResponse(array $ar_kanji): array
    {
        $url = $this->getServiceUrl($ar_kanji);
        $result = await($this->browser->get($url));

        if ($result->getStatusCode() != 200) {
            throw new DomainException('Error request. Code ' . $result->getStatusCode());
        }

        $response =  json_decode($result->getBody()->getContents(), true);

        if (count($response) < 1){
            throw new DomainException('Empty response');
        }

        return $response;
    }

    private function getServiceUrl( array $ar_kanji ): string {

        if ( env("JAPANESE_SENTENCE_PARSER_SERVICE_HOST") ){
            $host = 'http://' . env("JAPANESE_SENTENCE_PARSER_SERVICE_HOST");
        } else {
            $host = env("JAPANESE_SENTENCE_PARSER_URL");
        }

        $url = $host . "/?" . http_build_query([
            'kanji' => $ar_kanji
        ]);
        return preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $url);
    }

    private function createSentenceBox( string $kanji, array $sentences): SentenceBox
    {
        $box = new SentenceBox($kanji);
        foreach ( $sentences as $sentence ){
            $box->addSentence(new Sentence($sentence['japanese'], $sentence['english']));
        }
        return $box;
    }
}

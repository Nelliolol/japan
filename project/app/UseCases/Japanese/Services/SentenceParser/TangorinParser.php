<?php

namespace App\UseCases\Japanese\Services\SentenceParser;

use App\Services\HttpClient\HttpClient;
use App\UseCases\Japanese\Entities\Sentence;
use App\UseCases\Japanese\Entities\SentenceBox;
use KubAT\PhpSimple\HtmlDomParser;

class TangorinParser implements Parser
{
    private const SERVICE_URL = 'https://www.yarxi.ru/online/search.php';

    private HttpClient $http_client;

    public function __construct(HttpClient $http_client)
    {
        $this->http_client = $http_client;
    }

    public function parse( string $kanji): SentenceBox
    {
        $box = new SentenceBox($kanji);
        try {
            $dom = HtmlDomParser::str_get_html($this->getParceText($kanji));
            foreach ($dom->find('.results-dl .sentences') as $element) {
                foreach ($element->find('.s-jp ruby rt') ?? [] as $linkPart) {
                    $linkPart->plaintext = '';
                    $linkPart->innertext = '';
                }

                $japText = $element->find('.s-jp', 0)->plaintext;
                $engText = $element->find('.s-en', 0)->plaintext;

                $box->addSentence(new Sentence($japText, $engText));
            }
        } catch (\Exception $e) {}

        return $box;
    }

    private function getParceText($kanji): string
    {
        $result = $this->http_client->get(self::SERVICE_URL);

        if ($result->getStatusCode() != 200) {
            throw new \DomainException('Error request. Code ' . $result->getStatusCode());
        }

        return $result->getBody()->getContents();
    }
}

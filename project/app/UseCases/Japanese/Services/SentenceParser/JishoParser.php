<?php

namespace App\UseCases\Japanese\Services\SentenceParser;

use App\Services\HttpClient\HttpClient;
use App\UseCases\Japanese\Entities\Sentence;
use App\UseCases\Japanese\Entities\SentenceBox;
use DomainException;
use Exception;
use KubAT\PhpSimple\HtmlDomParser;

class JishoParser implements Parser
{
    private const SERVICE_URL = 'https://jisho.org/search/';

    private HttpClient $http_client;

    public function __construct(HttpClient $http_client)
    {
        $this->http_client = $http_client;
    }

    public function parse(string $kanji): SentenceBox
    {
        $box = new SentenceBox($kanji);
        try {
            $dom = HtmlDomParser::str_get_html($this->getParseText($kanji));
            foreach ($dom->find('ul.sentences .sentence_content') as $element) {
                $japText = '';
                foreach ($element->find('.unlinked') ?? [] as $linkPart) {
                    $japText .= $linkPart->plaintext;
                }
                $engText = $element->find('.english', 0)->plaintext;
                $box->addSentence(new Sentence($japText, $engText));
            }

        } catch (Exception $e) {}

        return $box;
    }

    /**
     * @param string[] $ar_kanji
     * @return SentenceBox[]
     */
    public function parseMany(array $ar_kanji): array
    {
        $result = [];
        foreach ($ar_kanji as $kanji) {
            $result[] = $this->parse($kanji);
        }

        return $result;
    }

    private function getParseText(string $kanji): string
    {
        $result = $this->http_client->get(self::SERVICE_URL . $kanji . '%20%23sentences');

        if ($result->getStatusCode() != 200) {
            throw new DomainException('Error request. Code ' . $result->getStatusCode());
        }

        return $result->getBody()->getContents();
    }
}

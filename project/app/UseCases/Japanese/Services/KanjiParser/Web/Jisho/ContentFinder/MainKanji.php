<?php

namespace App\UseCases\Japanese\Services\KanjiParser\Web\Jisho\ContentFinder;

use App\UseCases\Japanese\Entities\Kanji;
use App\UseCases\Japanese\Services\KanjiParser\Web\ContentFinder;

class MainKanji implements ContentFinder
{
    public function getKanji($dom): array
    {
        $finded = [];

        if ($mainKanji = $dom->find('.concept_light-representation', 0)) {

//            dump(smartTrim($mainKanji->find('.text', 0)->innertext));
            $data = new Kanji($mainKanji->find('.text', 0)->innertext);


            foreach ($dom->find('.concept_light')[0]->find('.meaning-meaning') as $on) {
                $data->addTranslate($on->plaintext);
            }

            if ($kun = $dom->find('.concept_light-status .f-dropdown li', 1)) {
                $data->addKun(str_replace('Sentence search for ', '', $kun->plaintext));
            }

            $finded[] = $data;
        }

        return $finded;
    }
}

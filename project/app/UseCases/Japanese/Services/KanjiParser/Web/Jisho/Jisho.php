<?php

namespace App\UseCases\Japanese\Services\KanjiParser\Web\Jisho;

use App\UseCases\Japanese\Services\KanjiParser\Web\WebParser;
use React\Promise\PromiseInterface;

class Jisho extends WebParser
{
    protected const SEARCH_URL = 'https://jisho.org/search/';

    protected function createHttpResponsePromise(string $kanji): PromiseInterface
    {
        return $this->browser->get(self::SEARCH_URL . $kanji);
    }
}

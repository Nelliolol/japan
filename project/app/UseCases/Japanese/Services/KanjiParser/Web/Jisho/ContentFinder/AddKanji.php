<?php

namespace App\UseCases\Japanese\Services\KanjiParser\Web\Jisho\ContentFinder;

use App\UseCases\Japanese\Entities\Kanji;
use App\UseCases\Japanese\Services\KanjiParser\Web\ContentFinder;

class AddKanji implements ContentFinder
{
    public function getKanji($dom): array
    {
        $finded = [];

        foreach ($dom->find('.entry.kanji_light') as $element) {

            $data = new Kanji($element->find('.character', 0)->plaintext);

            foreach ($element->find('.kun.readings .japanese_gothic') as $kun) {
                $data->addKun($kun->plaintext);
            }

            foreach ($element->find('.on.readings .japanese_gothic') as $on) {
                $data->addOn($on->plaintext);
            }

            $finded[] = $data;
        }

        return $finded;
    }
}

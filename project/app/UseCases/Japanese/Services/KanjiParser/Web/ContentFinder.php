<?php

namespace App\UseCases\Japanese\Services\KanjiParser\Web;

interface ContentFinder
{
    public function getKanji($dom): array;
}

<?php

namespace App\UseCases\Japanese\Services\KanjiParser\Web\Yarxi\ContentFinder;

use App\UseCases\Japanese\Entities\Kanji;
use App\UseCases\Japanese\Services\KanjiParser\Web\ContentFinder;

class WordPage implements ContentFinder
{
    public function getKanji($dom):array
    {
    	$finded = [];

        foreach($dom->find('.comp') as $element){

            $data = new Kanji( $element->find('.tjtext', 0)->plaintext );

            if ( $kun = $element->find('.ttrans', 0) ){
                $data->addReading( $kun->plaintext );
            }

            if ( $translates = $element->find('table tr') ){
                foreach ($translates as $translate) {
                    $data->addTranslate(smartTrim($translate->plaintext));
                }
            } else {

                $arTd = $element->find('td');
                // $translate = $element->find('td', 2);
                $translate = end($arTd);
                // dump($translate->plaintext);
                $data->addTranslate(smartTrim($translate->plaintext));
            }


            $finded[] = $data;
        }
        return $finded;
    }
}

<?php

namespace App\UseCases\Japanese\Services\KanjiParser\Web\Yarxi;

use App\UseCases\Japanese\Services\KanjiParser\Web\WebParser;
use React\Promise\PromiseInterface;

class Yarxi extends WebParser
{
    protected const SEARCH_URL = 'https://www.yarxi.ru/online/search.php';

    protected function createHttpResponsePromise(string $kanji): PromiseInterface
    {
        return $this->browser->post(
            self::SEARCH_URL,
            [
                'accept' => 'text/html, */*; q=0.01',
                'accept-encoding' => 'gzip, deflate, br',
                'accept-language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
                'content-type' => 'application/x-www-form-urlencoded',
                // 'content-length' => '52',
                // 'cookie' => 'sc_is_visitor_unique=rx4460896.1569650763.6B448E74DB934F9F90EB4E200D90DD3F.13.12.12.8.8.8.7.3.1',
                'origin' => 'https://yarxi.ru',
                'referer' => 'https://yarxi.ru',
                'sec-fetch-mode' => 'cors',
                'sec-fetch-site' => 'same-origin',
                'user-agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36',
                'x-requested-with' => 'XMLHttpRequest',
            ],
            http_build_query([
                'R' => $kanji,
                'M' => '',
                'Src' => 'bytext',
            ])
        );
    }
}

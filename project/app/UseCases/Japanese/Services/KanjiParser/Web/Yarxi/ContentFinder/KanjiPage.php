<?php

namespace App\UseCases\Japanese\Services\KanjiParser\Web\Yarxi\ContentFinder;

use App\UseCases\Japanese\Entities\Kanji;
use App\UseCases\Japanese\Services\KanjiParser\Web\ContentFinder;

class KanjiPage implements ContentFinder
{
    public function getKanji($dom): array
    {
    	$finded = [];

	    foreach($dom->find('.kun') as $element){

	        $data = new Kanji( $element->find('.kunj', 0)->plaintext );

	        if ( $kun = $element->find('.kunreading', 0) ){
	            $data->addReading( $kun->plaintext );
	        }

	        if ( $kun = $element->find('.kunreading_ch', 0) ){
	            $data->addReading( $kun->plaintext );
	        }

	        if ( $kun = $element->find('.kunreading_pale', 0) ){
	            $data->addReading( $kun->plaintext );
	        }


	        $kunForTableRow = $element->find('.kuntrans tr');
	        if ( !$kunForTableRow ){
	            $kunForTableRow = $element->find('.kuntrans');
	        }
	        foreach( $kunForTableRow as $translateElement){
	            $data->addTranslate( smartTrim($translateElement->plaintext) );
	        }

	        $finded[] = $data;
	    }
	    return $finded;
    }
}

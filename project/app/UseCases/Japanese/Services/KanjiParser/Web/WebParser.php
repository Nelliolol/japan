<?php

namespace App\UseCases\Japanese\Services\KanjiParser\Web;

use Exception;
use Psr\Http\Message\ResponseInterface;
use React\Http\Browser;
use Throwable;
use App\UseCases\Japanese\Entities\KanjiParseResult;
use App\UseCases\Japanese\Services\KanjiParser\Parser;
use DomainException;
use KubAT\PhpSimple\HtmlDomParser;
use React\Promise\PromiseInterface;
use function React\Async\await;
use function React\Promise\all;

abstract class WebParser implements Parser
{
    private array $finders;
    protected Browser $browser;

    public function __construct( Browser $browser )
    {
        $this->browser = $browser;
    }

    public function addContentFinder(ContentFinder $finder): void
    {
        $this->finders[] = $finder;
    }

    /**
     * @throws Throwable
     */
    public function parse(string $string_kanji): KanjiParseResult
    {
        $promise = $this->createParsePromise($string_kanji);
        return await($promise);
    }

    /**
     * @param string[] $ar_kanji
     * @return KanjiParseResult[]
     * @throws Throwable
     */
    public function parseMany(array $ar_kanji): array
    {
        $promises = [];
        foreach ($ar_kanji as $string_kanji) {
            $promises[] = $this->createParsePromise($string_kanji);
        }
        return await(all($promises));
    }
    protected function createParsePromise( string $kanji ): PromiseInterface
    {
        $kanji_parse_result = new KanjiParseResult(class_basename($this), $kanji);

        return $this->createHttpResponsePromise($kanji)->then(function (ResponseInterface $response) use ($kanji_parse_result) {
            try {
                foreach ($this->getResponseData((string)$response->getBody()) as $kanji) {
                    $kanji_parse_result->addkanji($kanji);
                }
            } catch (Exception $e) {
                $kanji_parse_result->setError($e->getMessage());
            }

            return $kanji_parse_result;

        }, function (Exception $e) use ($kanji_parse_result) {
            $kanji_parse_result->setError($e->getMessage());
            return $kanji_parse_result;
        });
    }

    abstract protected function createHttpResponsePromise(string $kanji): PromiseInterface;

    protected function getResponseData(string $dom): array
    {
        if (empty($this->finders)) {
            throw new DomainException('Finders not initiated');
        }

        $dom = HtmlDomParser::str_get_html($dom);

        $found = [];

        foreach ($this->finders as $finder) {
            $found = array_merge($finder->getKanji($dom), $found);
        }

        if (count($found) < 1) {
            throw new DomainException('Kanji not found.');
        }

        return $found;
    }
}

<?php

namespace App\UseCases\Japanese\Services\KanjiParser;

use App\UseCases\Japanese\Entities\KanjiParseResult;

interface Parser
{
    public function parse(string $string_kanji): KanjiParseResult;

    /**
     * @param string[] $ar_kanji
     * @return KanjiParseResult[]
     */
    public function parseMany(array $ar_kanji): array;
}

<?php

namespace App\UseCases\Japanese\Tests\Unit\Services\Export;

use App\UseCases\Japanese\Services\Export\JapaneseCardExporter;
use App\UseCases\Japanese\Tests\TestCase;

class JapaneseCardExporterTest extends TestCase
{
    private JapaneseCardExporter $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new JapaneseCardExporter;
    }

    public function testExport()
    {
        $tag = 'tag';
        $word_card_1 = $this->createWordCard();
        $word_card_2 = $this->createWordCard();
        $ar_words = [
            $word_card_1,
            $word_card_2
        ];

        $export_text = $this->service->renderExportFile($ar_words, $tag);

        $this->assertStringContainsString($word_card_1->getKanji(), $export_text);
        $this->assertStringContainsString($word_card_1->getReading(), $export_text);
        $this->assertStringContainsString($word_card_1->getTranslate(), $export_text);
        $this->assertStringContainsString($word_card_1->getExample(), $export_text);

        $this->assertStringContainsString($word_card_2->getKanji(), $export_text);
        $this->assertStringContainsString($word_card_2->getReading(), $export_text);
        $this->assertStringContainsString($word_card_2->getTranslate(), $export_text);
        $this->assertStringContainsString($word_card_2->getExample(), $export_text);
    }
}

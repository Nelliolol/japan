<?php

namespace App\UseCases\Japanese\Tests\Unit\Services\KanjiParser;

use App\UseCases\Japanese\Services\KanjiParser\Web\Yarxi\Yarxi;
use Tests\TestCase;

class YarxiKanjiParserTest extends TestCase
{
    private Yarxi $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->app->make(Yarxi::class);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testYarxiClearParseResult()
    {
        $testKanji = '養う';

        $parseResult = $this->service->parse($testKanji);
        $parsedKanji = null;
        foreach ( $parseResult->getKanji() as $kanji ){
            if ( $kanji->getName() == $testKanji ){
                $parsedKanji = $kanji;
                break;
            }
        }

        $this->assertNotNull($parsedKanji);
        $this->assertEquals('1. растить, воспитывать', $parsedKanji->getTranslate()[0]);
    }

    public function testYarxiParseType1()
    {
        $testKanji = '添';

        $parseResult = $this->service->parse($testKanji)->getKanji();

        $word = $parseResult[7];

        $this->assertNotNull($word->getReading());
    }

    public function testYarxiParseType2()
    {
        $testKanji = '添';

        $parseResult = $this->service->parse($testKanji)->getKanji();

        $word = $parseResult[8];

        $this->assertNotNull($word->getReading());
    }

    public function testYarxiParseType3()
    {
        $testKanji = '加';

        $parseResult = $this->service->parse($testKanji)->getKanji();

        $word = $parseResult[23];

        $this->assertNotNull($word->getReading());
    }

    public function testYarxiParseRemoveLineBreak()
    {
        $testKanji = '控える';

        $parseResult = $this->service->parse($testKanji)->getKanji();

        $word = $parseResult[0]->getTranslate()[0];

        $this->assertEquals('1. удерживаться от чего-л. быть воздержанным', $word);
    }

    public function testYarxiGetTranslate()
    {
        $testKanji = '街';

        $parseResult = $this->service->parse($testKanji)->getKanji();

        $word = $parseResult[4];

        $this->assertEquals('большая дорога', $word->getTranslate()[0]);
        $this->assertEquals('кварталы, городские улицы Ср. 町', $parseResult[0]->getTranslate()[0]);
    }
}

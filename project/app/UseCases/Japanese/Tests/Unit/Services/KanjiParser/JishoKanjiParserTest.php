<?php

namespace App\UseCases\Japanese\Tests\Unit\Services\KanjiParser;

use App\UseCases\Japanese\Services\KanjiParser\Web\Jisho\Jisho;
use Tests\TestCase;

class JishoKanjiParserTest  extends TestCase
{
    private Jisho $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->app->make(Jisho::class);
    }

    public function testParseManyKanji()
    {
        $ar_string_kanji = [
            '養う',
            '街',
            '立替',
            '平均',
            '該当',
            '適切',
            '抑々',
            '養う',
            '街',
        ];
        $parse_result = $this->service->parseMany($ar_string_kanji);
        $this->assertCount(9, $parse_result);
    }

    public function testParseOneKanji()
    {
        $kanji = '養う';
        $parse_result = $this->service->parse($kanji);
        $this->assertNotEmpty($parse_result->getKanji());
        $this->assertEquals($parse_result->getStringKanji(), $kanji);
    }

    public function testParseManyKanji_WhenNotFound()
    {
        $ar_string_kanji = [
            'not_exist_kanji',
        ];
        $parse_result = $this->service->parseMany($ar_string_kanji);
        $this->assertCount(1, $parse_result);
        $this->assertNotEmpty($parse_result[0]->getError());
    }
}

<?php

namespace App\UseCases\Japanese\Tests\Unit\Services\WordCardParser;

use App\UseCases\Japanese\Entities\Kanji;
use App\UseCases\Japanese\Entities\KanjiParseResult;
use App\UseCases\Japanese\Entities\Sentence;
use App\UseCases\Japanese\Entities\SentenceBox;
use App\UseCases\Japanese\Services\WordCardParser\YarxiAndJishoParser\WordCardFabric;
use App\UseCases\Japanese\Tests\TestCase;

class WordCardFabricTest extends TestCase
{
    public function testReturnNullWhenKanjiNotFound()
    {
        $service = $this->getService();
        $result = $service->createWordCard();
        $this->assertNull($result);
    }

    public function testFindKanjiNameWhenExistJisho()
    {
        $word = 'word';
        $jisho_name = 'jisho_name';
        $jisho_kenji = new Kanji($word);
        $jisho_kenji->addTranslate($jisho_name);

        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $service = $this->getService(
            $word,
            null,
            $jisho_kanji_parse_result
        );

        $word_card = $service->createWordCard();
        $this->assertEquals($word, $word_card->getKanji());
    }

    public function testFindKanjiNameWhenExistYarxi()
    {
        $word = 'word';
        $jisho_name = 'jisho_name';
        $yarxi_kenji = new Kanji($word);
        $yarxi_kenji->addTranslate($jisho_name);

        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$yarxi_kenji]);

        $service = $this->getService(
            $word,
            null,
            $jisho_kanji_parse_result,
        );

        $word_card = $service->createWordCard();
        $this->assertEquals($word, $word_card->getKanji());
    }

    public function testReturnNullWhenKanjiNameInNotSame()
    {
        $word = '食べる';
        $almost_same_word = '食べられる';
        $yarxi_kenji = new Kanji($almost_same_word);

        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$yarxi_kenji]);

        $service = $this->getService(
            $word,
            null,
            $jisho_kanji_parse_result,
        );

        $word_card = $service->createWordCard();
        $this->assertNull($word_card);
    }

    public function testReturnKanjiWithSameName()
    {
        $word = '食べる';
        $almost_same_word = '食べられる';
        $yarxi_kenji = new Kanji($word);
        $yarxi_kenji_2 = new Kanji($almost_same_word);

        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$yarxi_kenji, $yarxi_kenji_2]);

        $service = $this->getService(
            $word,
            null,
            $jisho_kanji_parse_result,
        );

        $word_card = $service->createWordCard();
        $this->assertEquals($word, $word_card->getKanji());
    }

    public function testFindReadingReturnJishoKunWhenExist()
    {
        $word = '食べる';

        $jisho_value = 'jisho_name';
        $jisho_kenji = new Kanji($word);
        $jisho_kenji->addKun($jisho_value);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $yarxi_value = 'yarxi_name';
        $yarxi_kenji = new Kanji($word);
        $yarxi_kenji->addReading($yarxi_value);
        $yarxi_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $yarxi_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$yarxi_kenji]);

        $service = $this->getService(
            $word,
            $yarxi_kanji_parse_result,
            $jisho_kanji_parse_result,
        );

        $word_card = $service->createWordCard();
        $this->assertEquals($jisho_value, $word_card->getReading());
    }

    public function testFindReadingReturnYarxiReadingWhenJishoNotExist()
    {
        $word = '食べる';

        $jisho_value = 'jisho_name';
        $jisho_kenji = new Kanji($word);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $yarxi_value = 'yarxi_name';
        $yarxi_kenji = new Kanji($word);
        $yarxi_kenji->addReading($yarxi_value);
        $yarxi_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $yarxi_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$yarxi_kenji]);

        $service = $this->getService(
            $word,
            $yarxi_kanji_parse_result,
            $jisho_kanji_parse_result,
        );

        $word_card = $service->createWordCard();
        $this->assertEquals($yarxi_value, $word_card->getReading());
    }

    public function testFindReadingReturnNullWhenNotExist()
    {
        $word = '食べる';

        $jisho_value = 'jisho_name';
        $jisho_kenji = new Kanji($word);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $yarxi_value = 'yarxi_name';
        $yarxi_kenji = new Kanji($word);
        $yarxi_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $yarxi_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$yarxi_kenji]);

        $service = $this->getService(
            $word,
            $yarxi_kanji_parse_result,
            $jisho_kanji_parse_result,
        );

        $word_card = $service->createWordCard();
        $this->assertNull($word_card->getReading());
    }

    public function testFindReadingWhenThereAreManyValues()
    {
        $word = '食べる';

        $jisho_kun_1 = 'taberu';
        $jisho_kun_2 = 'たべる';
        $jisho_kenji = new Kanji($word);
        $jisho_kenji->addKun($jisho_kun_1);
        $jisho_kenji->addKun($jisho_kun_2);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $service = $this->getService(
            $word,
            null,
            $jisho_kanji_parse_result
        );

        $reading = implode(', ', [$jisho_kun_1, $jisho_kun_2]);

        $word_card = $service->createWordCard();
        $this->assertEquals($reading, $word_card->getReading());
    }

    public function testFindTranslateReturnYarxiWhenJishoExist()
    {
        $word = '食べる';

        $jisho_value = 'jisho_name';
        $jisho_kenji = new Kanji($word);
        $jisho_kenji->addTranslate($jisho_value);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $yarxi_value = 'yarxi_name';
        $yarxi_kenji = new Kanji($word);
        $yarxi_kenji->addTranslate($yarxi_value);
        $yarxi_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $yarxi_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$yarxi_kenji]);

        $service = $this->getService(
            $word,
            $yarxi_kanji_parse_result,
            $jisho_kanji_parse_result,
        );

        $word_card = $service->createWordCard();
        $this->assertEquals($yarxi_value, $word_card->getTranslate());
    }

    public function testFindTranslateReturnJishoTranslateWhenYarxiNotExist()
    {
        $word = '食べる';

        $jisho_value = 'jisho_name';
        $jisho_kenji = new Kanji($word);
        $jisho_kenji->addTranslate($jisho_value);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $yarxi_value = 'yarxi_name';
        $yarxi_kenji = new Kanji($word);
        $yarxi_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $yarxi_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$yarxi_kenji]);

        $service = $this->getService(
            $word,
            $yarxi_kanji_parse_result,
            $jisho_kanji_parse_result,
        );

        $word_card = $service->createWordCard();
        $this->assertEquals($jisho_value, $word_card->getTranslate());
    }

    public function testFindTranslateReturnNullWhenNotExist()
    {
        $word = '食べる';

        $jisho_value = 'jisho_name';
        $jisho_kenji = new Kanji($word);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $yarxi_value = 'yarxi_name';
        $yarxi_kenji = new Kanji($word);
        $yarxi_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $yarxi_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$yarxi_kenji]);

        $service = $this->getService(
            $word,
            $yarxi_kanji_parse_result,
            $jisho_kanji_parse_result,
        );

        $word_card = $service->createWordCard();
        $this->assertNull($word_card->getTranslate());
    }

    public function testFindTranslateWhenThereAreManyValues()
    {
        $word = '食べる';

        $jisho_kun_1 = 'есть';
        $jisho_kun_2 = 'жрать';
        $jisho_kenji = new Kanji($word);
        $jisho_kenji->addTranslate($jisho_kun_1);
        $jisho_kenji->addTranslate($jisho_kun_2);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $service = $this->getService(
            $word,
            null,
            $jisho_kanji_parse_result
        );

        $reading = implode(', ', [$jisho_kun_1, $jisho_kun_2]);

        $word_card = $service->createWordCard();
        $this->assertEquals($reading, $word_card->getTranslate());
    }

    public function testFindExampleReturnNullWhenNoExamples()
    {
        $word = '食べる';
        $jisho_kenji = new Kanji($word);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $service = $this->getService(
            $word,
            null,
            $jisho_kanji_parse_result
        );

        $word_card = $service->createWordCard();
        $this->assertNull($word_card->getExample());
    }

    public function testFindExampleReturnShortestOne()
    {
        $word = '食べる';
        $jisho_kenji = new Kanji($word);
        $jisho_kanji_parse_result = $this->createMock(KanjiParseResult::class);
        $jisho_kanji_parse_result->expects($this->once())
            ->method('getKanji')
            ->willReturn([$jisho_kenji]);

        $sentence_box = new SentenceBox($word);
        $sentence_1 = 'abc';
        $sentence_2 = 'a';
        $sentence_3 = 'abcd';
        $sentence_box->addSentence(new Sentence($sentence_1, $sentence_1));
        $sentence_box->addSentence(new Sentence($sentence_2, $sentence_2));
        $sentence_box->addSentence(new Sentence($sentence_3, $sentence_3));

        $service = $this->getService(
            $word,
            null,
            $jisho_kanji_parse_result,
            $sentence_box
        );

        $word_card = $service->createWordCard();
        $this->assertEquals($sentence_2, $word_card->getExample());
    }

    private function getService(
        ?string           $word = null,
        ?KanjiParseResult $yarxi_kanji_parse_result = null,
        ?KanjiParseResult $jisho_kanji_parse_result = null,
        ?SentenceBox      $sentence_box = null
    ): WordCardFabric
    {
        if (is_null($word)) {
            $word = $this->faker->word;
        }

        return new WordCardFabric(
            $word,
            $yarxi_kanji_parse_result,
            $jisho_kanji_parse_result,
            $sentence_box
        );
    }
}

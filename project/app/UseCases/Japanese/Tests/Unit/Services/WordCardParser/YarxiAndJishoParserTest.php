<?php

namespace App\UseCases\Japanese\Tests\Unit\Services\WordCardParser;

use App\UseCases\Japanese\Entities\Kanji;
use App\UseCases\Japanese\Entities\KanjiParseResult;
use App\UseCases\Japanese\Entities\SentenceBox;
use App\UseCases\Japanese\Services\KanjiParser\Parser as KanjiParser;
use App\UseCases\Japanese\Services\SentenceParser\Parser as SentenceParser;
use App\UseCases\Japanese\Services\WordCardParser\YarxiAndJishoParser\YarxiAndJishoParser;
use App\UseCases\Japanese\Tests\TestCase;

class YarxiAndJishoParserTest extends TestCase
{
    public function testParseManyReturnNullWhenNotFoundWord()
    {
        $words_to_parse = [
          'word1',
          'word2',
        ];

        $yarxi_parser = $this->createMock(KanjiParser::class);
        $yarxi_parser->expects($this->exactly(1))
            ->method('parseMany')
            ->with($words_to_parse)
            ->willReturn([new KanjiParseResult('test','kanji')]);

        $jisho_parser = $this->createMock(KanjiParser::class);
        $jisho_parser->expects($this->exactly(1))
            ->method('parseMany')
            ->with($words_to_parse)
            ->willReturn([new KanjiParseResult('test', 'kanji')]);

        $sentence_parser = $this->createMock(SentenceParser::class);
        $sentence_parser->expects($this->exactly(1))
            ->method('parseMany')
            ->with($words_to_parse)
            ->willReturnOnConsecutiveCalls([new SentenceBox($words_to_parse[0]), new SentenceBox($words_to_parse[1])]);

        $service = $this->getService(
          $yarxi_parser,
          $jisho_parser,
          $sentence_parser
        );

        $result = $service->parseMany($words_to_parse);
        $this->assertEmpty($result);
    }

    public function testParseManyReturnArWordCard()
    {
        $word1 = 'word1';
        $word2 = 'word2';
        $words_to_parse = [
            $word1,
            $word2
        ];

        $parse_result = new KanjiParseResult($word1, $word1);
        $parse_result->addkanji(new Kanji($word1));
        $parse_result->addkanji(new Kanji($word2));

        $yarxi_parser = $this->createMock(KanjiParser::class);
        $yarxi_parser->expects($this->exactly(1))
            ->method('parseMany')
            ->with($words_to_parse)
            ->willReturn([$parse_result]);

        $sentence_parser = $this->createMock(SentenceParser::class);
        $sentence_parser->expects($this->exactly(1))
            ->method('parseMany')
            ->willReturn([new SentenceBox($word1)]);

        $service = $this->getService(
            $yarxi_parser,
            null,
            $sentence_parser
        );

        $result = $service->parseMany($words_to_parse);
        $this->assertCount(1, $result);
    }

    private function getService(
        ?KanjiParser $yarxi_parser = null,
        ?KanjiParser $jisho_parser = null,
        ?SentenceParser      $sentence_parser = null
    ): YarxiAndJishoParser
    {
        if (is_null($yarxi_parser)) {
            $yarxi_parser = $this->createMock(KanjiParser::class);
        }

        if (is_null($jisho_parser)) {
            $jisho_parser = $this->createMock(KanjiParser::class);
        }

        if (is_null($sentence_parser)) {
            $sentence_parser = $this->createMock(SentenceParser::class);
        }

        return new YarxiAndJishoParser(
            $yarxi_parser,
            $jisho_parser,
            $sentence_parser
        );
    }
}

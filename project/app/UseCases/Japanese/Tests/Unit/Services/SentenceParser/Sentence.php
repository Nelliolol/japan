<?php

namespace App\UseCases\Japanese\Tests\Unit\Services\SentenceParser;

class Sentence
{
    private string $japanese = '';
    private string $english = '';

    /**
     * @param string $japanese
     * @param string $english
     */
    public function __construct(string $japanese, string $english)
    {
        $this->japanese = $japanese;
        $this->english = $english;
    }

    /**
     * @return string
     */
    public function getJapanese(): string
    {
        return $this->japanese;
    }

    /**
     * @return string
     */
    public function getEnglish(): string
    {
        return $this->english;
    }
}

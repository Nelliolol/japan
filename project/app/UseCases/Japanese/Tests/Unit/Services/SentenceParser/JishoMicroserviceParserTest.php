<?php

namespace App\UseCases\Japanese\Tests\Unit\Services\SentenceParser;

use App\UseCases\Japanese\Services\SentenceParser\JishoMicroserviceParser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use React\Http\Browser;
use Tests\TestCase;

class JishoMicroserviceParserTest extends TestCase
{

    private JishoMicroserviceParser $service;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testParseOne()
    {
        $http_client = $this->prepareHttpClient(
            200,
            [
                (new ServiceResponseBodyBuilder())
                    ->setKanji('食べる')
                    ->addSentence("きょう何を昼食に食べましたか", "What did you have for lunch today?")
                    ->addSentence("がある条件を満たすと追ってくるモンスターを逆襲して食べることができる", "Pac-Man, when a certain condition is reached, can counter attack and eat the monsters chasing him.")
                    ->build(),
            ]);

        $this->service = new JishoMicroserviceParser($http_client);

        $response = $this->service->parse('食べる');
        if (mb_strpos($response->getSentence()[0]->getEngText(), 'today') !== false) {
            $this->assertEquals("What did you have for lunch today?", $response->getSentence()[0]->getEngText());
            $this->assertEquals("きょう何を昼食に食べましたか", $response->getSentence()[0]->getJapText());
        } else {
            $this->assertEquals("The most enjoyable part of traveling, after all, is eating the local specialties.", $response->getSentence()[0]->getEngText());
            $this->assertEquals("旅行の楽しみは、何といってもやはり、その土地の名物料理を食べることだろう。", $response->getSentence()[0]->getJapText());
        }
    }

    public function testParseMany()
    {
        $http_client = $this->prepareHttpClient(
            200,
            [
                (new ServiceResponseBodyBuilder())
                    ->setKanji('食べる')
                    ->addSentence("きょう何を昼食に食べましたか", "What did you have for lunch today?")
                    ->build(),
                (new ServiceResponseBodyBuilder())
                    ->setKanji('働く')
                    ->addSentence("きょう何を昼食に食べましたか", "What did you have for lunch today?")
                    ->build(),
            ]);

        $this->service = new JishoMicroserviceParser($http_client);

        $ar_kanji = [
            '食べる',
            '働く',
        ];

        $response = $this->service->parseMany($ar_kanji);
        $this->assertCount(2, $response);
    }


     public function testParseWhenEmptyWord()
     {
         $http_client = $this->prepareHttpClient(
             200,
             [
                 (new ServiceResponseBodyBuilder())
                     ->build(),
             ]);
         $this->service = new JishoMicroserviceParser($http_client);

         $response = $this->service->parse('');
         $this->assertEmpty($response->getSentence());
     }

     public function testParseWhenNotFound()
     {
         $http_client = $this->prepareHttpClient(
             200,
             [
                 (new ServiceResponseBodyBuilder())
                     ->setKanji('as23dfzdsfds')
                     ->build(),
             ]);

         $this->service = new JishoMicroserviceParser($http_client);

         $response = $this->service->parse('as23dfzdsfds');
         $this->assertEmpty($response->getSentence());
     }

    private function prepareHttpClient(int $code, array $body_to_json): Browser
    {
        $body_string = json_encode($body_to_json);
        $response = $this->makeResponse($code, $body_string);
        return $this->makeHttpClientWithResponse($response);
    }

    private function makeResponse(int $code, string $body_string = ''): ResponseInterface
    {
        $body = $this->createMock(StreamInterface::class);
        $body->method('getContents')
            ->willReturn($body_string);
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getStatusCode')
            ->willReturn($code);
        $response->method('getBody')
            ->willReturn($body);

        return $response;
    }

    private function makeHttpClientWithResponse(ResponseInterface $response): Browser
    {
        $promise_response = \React\Promise\resolve($response);
        $http_client = $this->createMock(Browser::class);
        $http_client->method('get')
            ->willReturn($promise_response);
        return $http_client;
    }
}

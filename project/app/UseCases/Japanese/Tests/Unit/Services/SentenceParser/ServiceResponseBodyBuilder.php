<?php

namespace App\UseCases\Japanese\Tests\Unit\Services\SentenceParser;

class ServiceResponseBodyBuilder
{
    private string $kanji = '';
    private string $error = '';
    /**
     * @var Sentence[]
     */
    private array $sentences = [];

    public function build(): object
    {
        return (object)[
            'kanji' => $this->kanji,
            'error' => $this->error,
            'sentences' => $this->getSentences(),
        ];
    }

    public function setKanji(string $kanji): self
    {
        $this->kanji = $kanji;
        return $this;
    }

    public function setError(string $error): self
    {
        $this->error = $error;
        return $this;
    }

    public function addSentence(string $japanese, string $english): ServiceResponseBodyBuilder
    {
        $this->sentences[] = new Sentence($japanese, $english);
        return $this;
    }

    /**
     * @return object[]|null
     */
    public function getSentences(): ?array
    {
        if (count($this->sentences) < 1) {
            return null;
        }

        return array_map(function ($sentence) {
            return (object)[
                'japanese' => $sentence->getJapanese(),
                'english' => $sentence->getEnglish(),
            ];
        }, $this->sentences);
    }
}

<?php

namespace App\UseCases\Japanese\Tests\Unit\Commands;

use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Response;
use App\UseCases\Japanese\Tests\TestCase;

class ExportResponseTest extends TestCase
{
    public function testGetCountWordsToExport()
    {
        $ar_words_to_export = ['test', 'export'];
        $service = new Response($ar_words_to_export, []);
        $this->assertEquals(count($ar_words_to_export), $service->countWordsToExport());
    }

    public function testGetCountParsedWords()
    {
        $ar_words_to_export = ['test', 'export'];
        $parsed_word_card = [
            $this->createWordCard(),
            $this->createWordCard(),
            $this->createWordCard(),
        ];
        $service = new Response($ar_words_to_export, $parsed_word_card);
        $this->assertEquals(count($parsed_word_card), $service->countParsedWords());
    }

    public function testGetNotParsedWordsCount()
    {
        $ar_words_to_export = ['test', 'export'];
        $parsed_word_card = [
            $this->createWordCard(),
        ];
        $service = new Response($ar_words_to_export, $parsed_word_card);
        $this->assertEquals(1, $service->countNotParsedWords());
    }

    public function testGetNotParsedWordsCountWhenZero()
    {
        $ar_words_to_export = ['test', 'export'];
        $parsed_word_card = [];
        $service = new Response($ar_words_to_export, $parsed_word_card);
        $this->assertEquals(2, $service->countNotParsedWords());
    }

    public function testGetNotFoundWordsWhenNotFoundAnything()
    {
        $ar_words_to_export = ['test', 'export'];
        $parsed_word_card = [];
        $service = new Response($ar_words_to_export, $parsed_word_card);
        $this->assertEquals($ar_words_to_export, $service->getNotFoundWords());
    }

    public function testGetNotFoundWordsWhenNotFoundPartly()
    {
        $ar_words_to_export = ['test', 'export'];
        $parsed_word_card = [
            $this->createWordCard('test'),
        ];
        $service = new Response($ar_words_to_export, $parsed_word_card);
        $this->assertEquals(['export'], $service->getNotFoundWords());
    }

    public function testGetNotFoundWordsWhenNotFoundAll()
    {
        $ar_words_to_export = ['test', 'export'];
        $parsed_word_card = [
            $this->createWordCard('test'),
            $this->createWordCard('export'),
        ];
        $service = new Response($ar_words_to_export, $parsed_word_card);
        $this->assertEquals([], $service->getNotFoundWords());
    }
}

<?php

namespace App\UseCases\Japanese\Tests\Unit\Commands;

use App\Services\FileHelper\FileHelper;
use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Command;
use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Handler;
use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Response;
use App\UseCases\Japanese\Services\Export\Exporter;
use App\UseCases\Japanese\Services\WordCardParser\Parser;
use App\UseCases\Japanese\Tests\TestCase;
use DomainException;

class GenerateAnkiExportFileTest extends TestCase
{
    public function testGetExceptionWhenCouldNotParseWords()
    {
        $command = $this->getCommand();

        $word_parser = $this->createMock(Parser::class);
        $word_parser->expects($this->once())
            ->method('parseMany')
            ->with($command->getArWordsToExport())
            ->willReturn([]);

        $handler = $this->getHandler($word_parser);
        $this->expectException(DomainException::class);
        $handler->handle($command);
    }

    public function testGenerateFile()
    {
        $command = $this->getCommand();

        $parsed_words = [$this->createWordCard()];
        $word_parser = $this->createMock(Parser::class);
        $word_parser->expects($this->once())
            ->method('parseMany')
            ->willReturn($parsed_words);

        $export_text = 'test';
        $exporter = $this->createMock(Exporter::class);
        $exporter->expects($this->once())
            ->method('renderExportFile')
            ->with($parsed_words, $command->getTag())
            ->willReturn($export_text);

        $file_helper = $this->createMock(FileHelper::class);
        $file_helper->expects($this->once())
            ->method('put')
            ->with($command->getPathToExportFile(), $export_text);


        $handler = $this->getHandler(
            $word_parser,
            $exporter,
            $file_helper
        );
        $handler->handle($command);
    }

    public function testReturnResponse()
    {
        $command = $this->getCommand();

        $parsed_words = [$this->createWordCard()];
        $word_parser = $this->createMock(Parser::class);
        $word_parser->expects($this->once())
            ->method('parseMany')
            ->willReturn($parsed_words);

        $export_text = 'test';
        $exporter = $this->createMock(Exporter::class);
        $exporter->expects($this->once())
            ->method('renderExportFile')
            ->with($parsed_words, $command->getTag())
            ->willReturn($export_text);

        $file_helper = $this->createMock(FileHelper::class);
        $file_helper->expects($this->once())
            ->method('put')
            ->with($command->getPathToExportFile(), $export_text);


        $handler = $this->getHandler(
            $word_parser,
            $exporter,
            $file_helper
        );
        $response = $handler->handle($command);
        $this->assertInstanceOf(Response::class, $response);
    }

    private function getCommand(
        ?array  $ar_words_to_export = null,
        ?string $tag = null,
        ?string $path_to_export_file = null
    ): Command
    {
        if (is_null($tag)) {
            $tag = 'test';
        }

        if (is_null($ar_words_to_export)) {
            $ar_words_to_export = ['test', 'export'];
        }

        if (is_null($path_to_export_file)) {
            $path_to_export_file = '/';
        }

        $command = new Command(
            $tag,
            $ar_words_to_export,
            $path_to_export_file
        );

        return $command;
    }

    private function getHandler(
        ?Parser     $word_parser = null,
        ?Exporter   $exporter = null,
        ?FileHelper $file_helper = null
    ): Handler
    {
        if (is_null($word_parser)) {
            $word_parser = $this->createMock(Parser::class);
        }

        if (is_null($exporter)) {
            $exporter = $this->createMock(Exporter::class);
        }

        if (is_null($file_helper)) {
            $file_helper = $this->createMock(FileHelper::class);
        }

        return new Handler(
            $word_parser,
            $exporter,
            $file_helper
        );
    }
}

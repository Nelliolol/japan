<?php

namespace App\UseCases\Japanese\Tests\Unit\Queries;

use App\UseCases\Japanese\Entities\KanjiParseResult;
use App\UseCases\Japanese\Entities\SentenceBox;
use App\UseCases\Japanese\Queries\GetKanjiInformation\Command;
use App\UseCases\Japanese\Queries\GetKanjiInformation\Handler;
use App\UseCases\Japanese\Services\KanjiParser\Parser as KanjiParser;
use App\UseCases\Japanese\Services\SentenceParser\Parser as SentenceParser;
use App\UseCases\Japanese\Tests\TestCase;

class GetKanjiInformationTest extends TestCase
{
    public function testCallFunctions()
    {
        $search_kanji = '食べる';
        $ar_kanji_parsers = [];

        $kanji_parse_result_1 = new KanjiParseResult('test1', 'kanji');
        $kanji_parse_result_2 = new KanjiParseResult('test2', 'kanji');
        $sentence_parse_result = new SentenceBox($search_kanji);

        $yarxi_parser = $this->createMock(KanjiParser::class);
        $yarxi_parser->expects($this->once())
            ->method('parse')
            ->with($search_kanji)
            ->willReturn($kanji_parse_result_1);
        $ar_kanji_parsers[] = $yarxi_parser;

        $jisho_parser = $this->createMock(KanjiParser::class);
        $jisho_parser->expects($this->once())
            ->method('parse')
            ->with($search_kanji)
            ->willReturn($kanji_parse_result_2);
        $ar_kanji_parsers[] = $jisho_parser;

        $sentence_parser = $this->createMock(SentenceParser::class);
        $sentence_parser->expects($this->once())
            ->method('parse')
            ->with($search_kanji)
            ->willReturn($sentence_parse_result);

        $service = $this->getHandler(
            $ar_kanji_parsers,
            $sentence_parser
        );

        $result = $service->handle(new Command($search_kanji));

        $this->assertEquals($sentence_parse_result, $result->getParsedSentences());
        $this->assertEquals([$kanji_parse_result_1, $kanji_parse_result_2], $result->getArKanjiParseResults());
    }

    /**
     * @param KanjiParser[]|null $ar_kanji_parsers
     * @param SentenceParser|null $sentence_parser
     * @return Handler
     */
    private function getHandler(
        array $ar_kanji_parsers = null,
        ?SentenceParser $sentence_parser = null
    ): Handler
    {
        if (is_null($ar_kanji_parsers)) {
            $ar_kanji_parsers[] = $this->createMock(KanjiParser::class);
        }

        if (is_null($sentence_parser)) {
            $sentence_parser = $this->createMock(SentenceParser::class);
        }

        return new Handler(
            $ar_kanji_parsers,
            $sentence_parser
        );
    }
}

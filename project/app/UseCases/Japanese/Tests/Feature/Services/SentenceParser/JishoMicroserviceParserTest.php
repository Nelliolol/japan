<?php

namespace App\UseCases\Japanese\Tests\Feature\Services\SentenceParser;

use App\UseCases\Japanese\Services\SentenceParser\JishoMicroserviceParser;
use React\Http\Browser;
use Tests\TestCase;

class JishoMicroserviceParserTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new JishoMicroserviceParser(new Browser);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testParseOne()
    {
        $response = $this->service->parse('食べる');
        if ( mb_strpos($response->getSentence()[0]->getEngText(), 'today') !== false ) {
            $this->assertEquals("What did you have for lunch today?", $response->getSentence()[0]->getEngText());
            $this->assertEquals("きょう何を昼食に食べましたか", $response->getSentence()[0]->getJapText());
        } else {
            $this->assertEquals("The most enjoyable part of traveling, after all, is eating the local specialties.", $response->getSentence()[0]->getEngText());
            $this->assertEquals("旅行の楽しみは、何といってもやはり、その土地の名物料理を食べることだろう。", $response->getSentence()[0]->getJapText());
        }
    }
}

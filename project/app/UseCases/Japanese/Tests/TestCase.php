<?php

namespace App\UseCases\Japanese\Tests;

use App\UseCases\Japanese\Entities\WordCard;

class TestCase extends \Tests\TestCase
{
    public function createWordCard(
        ?string $kanji = null,
        ?string $reading = null,
        ?string $translate = null,
        ?string $example = null
    ): WordCard
    {
        if (is_null($kanji)) {
            $kanji = $this->faker->word;
        }

        if (is_null($reading)) {
            $reading = $this->faker->word;
        }

        if (is_null($translate)) {
            $translate = $this->faker->word;
        }

        if (is_null($example)) {
            $example = $this->faker->word;
        }

        return (new WordCard($kanji))
            ->setReading($reading)
            ->setTranslate($translate)
            ->setExample($example);
    }
}

<?php

namespace App\UseCases\Japanese\Entities;

class SentenceBox
{
    private string $kanji;

    /**
     * @var Sentence[]
     */
    private array $ar_sentences = [];

    /**
     * @param string $kanji
     */
    public function __construct(string $kanji)
    {
        $this->kanji = $kanji;
    }

    public function addSentence( Sentence $sentence ): void
    {
        $this->ar_sentences[] = $sentence;
    }

    /**
     * @return Sentence[]
     */
    public function getSentence(): array
    {
        return $this->ar_sentences;
    }

    /**
     * @return string
     */
    public function getKanji(): string
    {
        return $this->kanji;
    }
}

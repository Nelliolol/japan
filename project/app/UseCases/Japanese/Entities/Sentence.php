<?php

namespace App\UseCases\Japanese\Entities;

class Sentence
{
    private string $jap;
    private string $eng;

	public function __construct( string $jap, string $eng )
	{
		$this->jap = $jap;
        $this->eng = $eng;
	}

    public function getJapText(): string
    {
        return $this->jap;
    }

    public function getEngText(): string
    {
        return $this->eng;
    }
}

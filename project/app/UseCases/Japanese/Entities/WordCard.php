<?php

namespace App\UseCases\Japanese\Entities;

class WordCard
{
    private WordCardField $kanji;
    private ?WordCardField $reading = null;
    private ?WordCardField $translate = null;
    private ?WordCardField $example = null;

    public function __construct(string $kanji)
    {
        $this->kanji = new WordCardField($kanji);
    }

    public function getKanji(): string
    {
        return $this->kanji->getValue();
    }

    public function setReading(string $reading): WordCard
    {
        $this->reading = new WordCardField($reading);
        return $this;
    }

    public function getReading(): ?string
    {
        return $this->reading ? $this->reading->getValue() : null;
    }

    public function setTranslate(string $translate): WordCard
    {
        $this->translate = new WordCardField($translate);
        return $this;
    }

    public function getTranslate(): ?string
    {
        return $this->translate ? $this->translate->getValue() : null;
    }

    public function setExample(string $example): WordCard
    {
        $this->example = new WordCardField($example);
        return $this;
    }

    public function getExample(): ?string
    {
        return $this->example ? $this->example->getValue() : null;
    }
}

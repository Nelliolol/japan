<?php

namespace App\UseCases\Japanese\Entities;

class Kanji
{
    private WordCardField $name;

    /**
     * @var string[]
     */
    private array $kun = [];

    /**
     * @var string[]
     */
    private array $on = [];

    /**
     * @var string[]
     */
    private array $translate = [];
    private string $reading = '';

    public function __construct(string $name)
    {
        $this->name = new WordCardField($name);
    }

    public function addKun(string $text): void
    {
        $this->kun[] = $text;
    }

    public function addOn(string $text): void
    {
        $this->on[] = $text;
    }

    public function addTranslate(string $translate): void
    {
        $this->translate[] = $translate;
    }

    public function addReading(string $reading): void
    {
        $this->reading = $reading;
    }

    public function getName(): string
    {
        return $this->name->getValue();
    }

    /**
     * @return string[]
     */
    public function getKun(): array
    {
        return $this->kun;
    }

    /**
     * @return string[]
     */
    public function getOn(): array
    {
        return $this->on;
    }

    public function getReading(): string
    {
        return $this->reading;
    }

    /**
     * @return string[]
     */
    public function getTranslate(): array
    {
        return $this->translate;
    }
}

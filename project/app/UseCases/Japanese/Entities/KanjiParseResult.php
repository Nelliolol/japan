<?php

namespace App\UseCases\Japanese\Entities;

class KanjiParseResult
{
    private string $parser_name;
    private string $string_kanji;
    /**
     * @var Kanji[]
     */
    private array $ar_parsed_kanji = [];
    private string $error = '';

    public function __construct(string $parser_name, string $string_kanji)
    {
        $this->parser_name = $parser_name;
        $this->string_kanji = $string_kanji;
    }

    public function addkanji(Kanji $kanji): void
    {
        $this->ar_parsed_kanji[] = $kanji;
    }

    public function setError(string $error): void
    {
        $this->error = $error;
    }

    public function isError(): bool
    {
        return !empty($this->error);
    }

    public function getParserName(): string
    {
        return $this->parser_name;
    }

    /**
     * @return Kanji[]
     */
    public function getKanji(): array
    {
        return $this->ar_parsed_kanji;
    }

    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getStringKanji(): string
    {
        return $this->string_kanji;
    }
}

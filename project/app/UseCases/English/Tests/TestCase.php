<?php

namespace App\UseCases\English\Tests;

use App\UseCases\English\Entities\WordCard;

class TestCase extends \Tests\TestCase
{
    public function createWordCard(
        ?string $word = null,
        ?string $transcription = null,
        ?string $noun = null,
        ?string $verb = null,
        ?string $example = null
    ): WordCard
    {
        if (is_null($word)) {
            $word = $this->faker->word;
        }

        if (is_null($transcription)) {
            $transcription = $this->faker->word;
        }

        if (is_null($noun)) {
            $noun = $this->faker->word;
        }

        if (is_null($verb)) {
            $verb = $this->faker->word;
        }

        if (is_null($example)) {
            $example = $this->faker->word;
        }

        return (new WordCard($word))
            ->setTranscription($transcription)
            ->addTranslateNoun($noun)
            ->addTranslateVerb($verb)
            ->setExample($example);
    }
}

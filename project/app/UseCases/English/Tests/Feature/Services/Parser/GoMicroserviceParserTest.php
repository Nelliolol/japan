<?php

namespace App\UseCases\English\Tests\Feature\Services\Parser;

use App\Services\HttpClient\GuzzleAdapter;
use App\UseCases\English\Services\Parser\GoMicroserviceParser;
use Tests\TestCase;

class GoMicroserviceParserTest extends TestCase
{
    protected function setUp():void
    {
        parent::setUp();
        $this->service = new GoMicroserviceParser(new GuzzleAdapter);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testParseOne()
    {
        $english_word = $this->service->parse('envy');
        $this->assertEquals('envy', $english_word->getWord());
        $this->assertEquals('|ˈenvɪ|', $english_word->getTranscription());
        $this->assertEquals('зависть', $english_word->getTranslateNoun()[0]);
        $this->assertEquals('завидовать', $english_word->getTranslateVerb()[0]);
        $this->assertEmpty($english_word->getTranslateAdjective());
        $this->assertEquals('They envy us our new house.', $english_word->getExample());
    }
}

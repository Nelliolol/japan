<?php

namespace App\UseCases\English\Tests\Unit\Commands;

use App\Services\FileHelper\DTO\DownloadTask;
use App\Services\FileHelper\FileHelper;
use App\Services\UUIDGenerator\UUIDGenerator;
use App\UseCases\English\Commands\GenerateAnkiExportFile\Command;
use App\UseCases\English\Commands\GenerateAnkiExportFile\Handler;
use App\UseCases\English\Commands\GenerateAnkiExportFile\Response;
use App\UseCases\English\Services\Export\Exporter;
use App\UseCases\English\Services\Parser\Parser;
use App\UseCases\English\Tests\TestCase;
use DomainException;

class GenerateAnkiExportFileTest extends TestCase
{
    public function testGetExceptionWhenCouldNotParseWords()
    {
        $command = $this->getCommand();

        $word_parser = $this->createMock(Parser::class);
        $word_parser->expects($this->once())
            ->method('parseMany')
            ->with($command->getArWordsToExport())
            ->willReturn([]);

        $handler = $this->getHandler($word_parser);
        $this->expectException(DomainException::class);
        $handler->handle($command);
    }

    public function testGenerateFileWithOutDownloadingAudio()
    {
        $command = $this->getCommand();

        $parsed_words = [$this->createWordCard()];
        $word_parser = $this->createMock(Parser::class);
        $word_parser->expects($this->once())
            ->method('parseMany')
            ->willReturn($parsed_words);

        $export_text = 'test';
        $exporter = $this->createMock(Exporter::class);
        $exporter->expects($this->once())
            ->method('renderExportFile')
            ->with($parsed_words, $command->getTag())
            ->willReturn($export_text);

        $file_helper = $this->createMock(FileHelper::class);
        $file_helper->expects($this->once())
            ->method('put')
            ->with($command->getPathToExportFile(), $export_text);


        $handler = $this->getHandler(
            $word_parser,
            $exporter,
            $file_helper
        );
        $handler->handle($command);
    }

    public function testGenerateWithEnableDownloadingAudioFilesAndNoAudionUrlWereParsed()
    {
        $command = $this->getCommand();
        $command->enableDownloadingAudioFiles();

        $parsed_words = [$this->createWordCard()];
        $word_parser = $this->createMock(Parser::class);
        $word_parser->expects($this->once())
            ->method('parseMany')
            ->willReturn($parsed_words);

        $export_text = 'test';
        $exporter = $this->createMock(Exporter::class);
        $exporter->expects($this->once())
            ->method('renderExportFile')
            ->with($parsed_words, $command->getTag())
            ->willReturn($export_text);

        $file_helper = $this->createMock(FileHelper::class);
        $file_helper->expects($this->once())
            ->method('put')
            ->with($command->getPathToExportFile(), $export_text);
        $file_helper->expects($this->never())
            ->method('deleteDirectory');
        $file_helper->expects($this->never())
            ->method('downloadFiles');

        $handler = $this->getHandler(
            $word_parser,
            $exporter,
            $file_helper
        );
        $handler->handle($command);
    }

    public function testGenerateWithEnableDownloadingAudioFilesAndAudionUrlWereParsed()
    {
        $command = $this->getCommand();
        $command->enableDownloadingAudioFiles();

        $word_card = $this->createWordCard();
        $word_card->addAudioDownloadUrl("http://www.Murray.info/");
        $word_card_2 = $this->createWordCard();
        $parsed_words = [$word_card, $word_card_2];
        $word_parser = $this->createMock(Parser::class);
        $word_parser->expects($this->once())
            ->method('parseMany')
            ->willReturn($parsed_words);

        $generated_uuid = 'uuid';
        $audio_file_name = $generated_uuid . '.mp3';
        $audio_file_path = $command->getPathToAudioDirectory() . $audio_file_name;
        $ar_download_task = [
            new DownloadTask(
                $word_card->getAudioDownloadUrl(),
                $audio_file_path
            )
        ];

        $file_helper = $this->createMock(FileHelper::class);
        $file_helper->expects($this->once())
            ->method('put');
        $file_helper->expects($this->once())
            ->method('deleteDirectory')
            ->with($command->getPathToAudioDirectory());
        $file_helper->expects($this->once())
            ->method('downloadFiles')
            ->with($ar_download_task);
        $file_helper->expects($this->once())
            ->method('exists')
            ->with($audio_file_path)
            ->willReturn(true);

        $UUID_generator = $this->createMock(UUIDGenerator::class);
        $UUID_generator->expects($this->once())
            ->method('generate')
            ->willReturn($generated_uuid);

        $download_file_name_before = $word_card->getDownloadedAudioFileName();

        $handler = $this->getHandler(
            $word_parser,
            null,
            $file_helper,
            $UUID_generator
        );
        $handler->handle($command);

        $download_file_name_after = $word_card->getDownloadedAudioFileName();

        $this->assertNotEquals($download_file_name_before, $download_file_name_after);
        $this->assertEquals($audio_file_name, $download_file_name_after);

        $this->assertEquals('', $word_card_2->getDownloadedAudioFileName());
    }

    public function testReturnResponse()
    {
        $command = $this->getCommand();

        $parsed_words = [$this->createWordCard()];
        $word_parser = $this->createMock(Parser::class);
        $word_parser->expects($this->once())
            ->method('parseMany')
            ->willReturn($parsed_words);

        $export_text = 'test';
        $exporter = $this->createMock(Exporter::class);
        $exporter->expects($this->once())
            ->method('renderExportFile')
            ->with($parsed_words, $command->getTag())
            ->willReturn($export_text);

        $file_helper = $this->createMock(FileHelper::class);
        $file_helper->expects($this->once())
            ->method('put')
            ->with($command->getPathToExportFile(), $export_text);


        $handler = $this->getHandler(
            $word_parser,
            $exporter,
            $file_helper
        );
        $response = $handler->handle($command);
        $this->assertInstanceOf(Response::class, $response);
    }

    private function getCommand(
        ?array  $ar_words_to_export = null,
        ?string $tag = null,
        ?string $path_to_export_file = null,
        ?string $path_to_audio_directory = null,
        ?bool   $download_audio = false,
        ?bool   $debug = false
    ): Command
    {
        if (is_null($tag)) {
            $tag = 'test';
        }

        if (is_null($ar_words_to_export)) {
            $ar_words_to_export = ['test', 'export'];
        }

        if (is_null($path_to_export_file)) {
            $path_to_export_file = '/';
        }

        if (is_null($path_to_audio_directory)) {
            $path_to_audio_directory = '/';
        }

        $command = new Command(
            $tag,
            $ar_words_to_export,
            $path_to_export_file,
            $path_to_audio_directory
        );

        if ($download_audio) {
            $command->enableDownloadingAudioFiles();
        } else {
            $command->disableDownloadingAudioFiles();
        }

        if ($debug) {
            $command->enableDebugging();
        } else {
            $command->disableDebugging();
        }

        return $command;
    }

    private function getHandler(
        ?Parser $word_parser = null,
        ?Exporter          $exporter = null,
        ?FileHelper        $file_helper = null,
        ?UUIDGenerator     $UUID_generator = null
    ): Handler
    {
        if (is_null($word_parser)) {
            $word_parser = $this->createMock(Parser::class);
        }

        if (is_null($exporter)) {
            $exporter = $this->createMock(Exporter::class);
        }

        if (is_null($file_helper)) {
            $file_helper = $this->createMock(FileHelper::class);
        }

        if (is_null($UUID_generator)) {
            $UUID_generator = $this->createMock(UUIDGenerator::class);
        }

        return new Handler(
            $word_parser,
            $exporter,
            $file_helper,
            $UUID_generator
        );
    }
}

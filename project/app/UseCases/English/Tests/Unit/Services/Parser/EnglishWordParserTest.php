<?php

namespace App\UseCases\English\Tests\Unit\Services\Parser;

use App\Services\HttpClient\GuzzleAdapter;
use App\UseCases\English\Services\Parser\Paragraph77\Paragraph77Parser;
use DomainException;
use Tests\TestCase;

class EnglishWordParserTest extends TestCase
{
    protected function setUp():void
    {
        parent::setUp();
        $this->service = new Paragraph77Parser(new GuzzleAdapter);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testParseWithOutAdjectiveTranslate()
    {
        $english_word = $this->service->parse('envy');
        $this->assertEquals('envy', $english_word->getWord());
        $this->assertEquals('|ˈenvɪ|', $english_word->getTranscription());
        $this->assertEquals('зависть', $english_word->getTranslateNoun()[0]);
        $this->assertEquals('завидовать', $english_word->getTranslateVerb()[0]);
        $this->assertEmpty($english_word->getTranslateAdjective());
        $this->assertEquals('They envy us our new house.', $english_word->getExample());
    }

    public function testParseMany()
    {
        $ar_words = [
          'envy',
          'cruel',
        ];

        $ar_word_cards = $this->service->parseMany($ar_words);
        $this->assertCount(2, $ar_word_cards);
        $this->assertEquals($ar_words[0], $ar_word_cards[0]->getWord());
        $this->assertEquals($ar_words[1], $ar_word_cards[1]->getWord());
    }

    public function testParseWithOutClickableTranslate()
    {
        $english_word = $this->service->parse('cruel');

        $this->assertEquals('cruel', $english_word->getWord());
        $this->assertEquals('|ˈkruːəl|', $english_word->getTranscription());
        $this->assertEquals('жестокий, безжалостный, бессердечный', $english_word->getTranslateAdjective()[0]);
        $this->assertEquals('мучительный, ужасный, жестокий', $english_word->getTranslateAdjective()[1]);
        $this->assertEquals('в грам. знач. нареч. эмоц.-усил. очень, чрезвычайно, чертовски', $english_word->getTranslateAdjective()[2]);
        $this->assertEquals('австрал. разг. портить, грубить, лишать шансов на успех', $english_word->getTranslateVerb()[0]);
        $this->assertEmpty($english_word->getTranslateNoun());
        $this->assertNotEmpty($english_word->getExample());
    }

    public function testParseWithTwoWordsWhenFound()
    {
        $english_word = $this->service->parse('get on');

        $this->assertEquals('get on', $english_word->getWord());
        $this->assertEquals('|ˈɡet ɑːn|', $english_word->getTranscription());

        $this->assertEquals('надевать', $english_word->getTranslateVerb()[0]);

        $this->assertEquals('Do your children get on?', $english_word->getExample());
    }

    public function testParseWithTwoWordsWhenNotFound()
    {
        $english_word = $this->service->parse('looking forward');

        $this->assertEquals('looking forward', $english_word->getWord());
        $this->assertEmpty($english_word->getTranscription());
        $this->assertEquals('жду с нетерпением', $english_word->getTranslateVerb()[0]);
    }

    public function testParsePredlog()
    {
        $english_word = $this->service->parse('towards');

        $this->assertCount(5, $english_word->getTranslateAdjective());

    }

    public function testParseWhenEmptyWord()
    {
        $this->expectException( DomainException::class );
        $this->service->parse('');
    }

    public function testParseWhenNotFound()
    {
        $this->expectException( DomainException::class );
        $this->service->parse('as23dfzdsfds');
    }

    public function testParseWordWithAppersant()
    {
        $english_word = $this->service->parse("o'clock");
        $this->assertEquals("o'clock", $english_word->getWord());
        $this->assertEquals('|əˈklɑːk|', $english_word->getTranscription());
        $this->assertEquals('по часам, на часах', $english_word->getTranslateAdjective()[0]);
        $this->assertEquals("It's exactly 3 o'clock.", $english_word->getExample());
    }

    public function testExportWithHiddenData()
    {
        $english_word = $this->service->parse("bare");
        $this->assertCount(5, $english_word->getTranslateAdjective());
        $this->assertCount(4, $english_word->getTranslateVerb());
    }

    public function testGetAudio()
    {
        $english_word = $this->service->parse("bare");
        $this->assertNotEmpty($english_word->getAudioDownloadUrl());
    }

    public function testParseWhenExistSpanAndExistWordWithOutSpan()
    {
        $english_word = $this->service->parse("betrayal");
        $this->assertEquals('измена, предательство', $english_word->getTranslateNoun()[0]);
        $this->assertEquals("признак (чего-л.)", $english_word->getTranslateNoun()[1]);
    }

    public function testPearceWhenWordIsUpperCase()
    {
        $english_word = $this->service->parse("Betrayal");
        $this->assertEquals('измена, предательство', $english_word->getTranslateNoun()[0]);
        $this->assertEquals("признак (чего-л.)", $english_word->getTranslateNoun()[1]);
    }
}

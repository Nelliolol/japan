<?php

namespace App\UseCases\English\Tests\Unit\Services\Parser;

use App\Services\HttpClient\HttpClient;
use App\UseCases\English\Services\Parser\GoMicroserviceParser;
use DomainException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Tests\TestCase;

class GoMicroserviceParserTest extends TestCase
{
    private GoMicroserviceParser $service;

    public function testParseOne()
    {
        $http_client = $this->prepareHttpClient(
            200,
            [
                (new ServiceResponseBodyBuilder())
                    ->setWord('envy')
                    ->setTranscription('|ˈenvɪ|')
                    ->setTranslateNoun("зависть")
                    ->setTranslateNoun("предмет зависти")
                    ->setTranslateVerb("завидовать")
                    ->setExample("They envy us our new house.")
                    ->setAudioDownloadUrl("https://wooordhunt.ru/data/sound/sow/us/envy.mp3")
                    ->build()
            ]);

        $this->service = new GoMicroserviceParser($http_client);

        $english_word = $this->service->parse('envy');
        $this->assertEquals('envy', $english_word->getWord());
        $this->assertEquals('|ˈenvɪ|', $english_word->getTranscription());
        $this->assertEquals('зависть', $english_word->getTranslateNoun()[0]);
        $this->assertEquals('завидовать', $english_word->getTranslateVerb()[0]);
        $this->assertEmpty($english_word->getTranslateAdjective());
        $this->assertEquals('They envy us our new house.', $english_word->getExample());
    }

    public function testParseMany()
    {
        $http_client = $this->prepareHttpClient(
            200,
            [
                (new ServiceResponseBodyBuilder())
                    ->setWord('envy')
                    ->setTranscription('|ˈenvɪ|')
                    ->setTranslateNoun("зависть")
                    ->setTranslateNoun("предмет зависти")
                    ->setTranslateVerb("завидовать")
                    ->setExample("They envy us our new house.")
                    ->setAudioDownloadUrl("https://wooordhunt.ru/data/sound/sow/us/envy.mp3")
                    ->build(),
                (new ServiceResponseBodyBuilder())
                    ->setWord('Cruel')
                    ->setTranscription('|ˈkruːəl|')
                    ->setTranslateVerb("австрал. разг. портить, грубить, лишать шансов на успех")
                    ->setTranslateAdjective("жестокий, безжалостный, бессердечный")
                    ->setTranslateAdjective("мучительный, ужасный, жестокий")
                    ->setTranslateAdjective("в грам. знач. нареч. эмоц.-усил. очень, чрезвычайно, чертовски")
                    ->setExample("She was often cruel to her sister.")
                    ->setAudioDownloadUrl("https://wooordhunt.ru/data/sound/sow/us/cruel.mp3")
                    ->build(),
                (new ServiceResponseBodyBuilder())
                    ->setWord('winter')
                    ->setTranscription('|ˈwɪntər|')
                    ->setTranslateNoun("зима")
                    ->setTranslateNoun("год (жизни)")
                    ->setTranslateNoun("период застоя, упадка")
                    ->setExample("Winter has set in.")
                    ->setAudioDownloadUrl("https://wooordhunt.ru/data/sound/sow/us/winter.mp3")
                    ->build(),
            ],
        );

        $this->service = new GoMicroserviceParser($http_client);

        $ar_words = [
            'envy',
            'Cruel',
            'winter',
        ];

        $ar_word_cards = $this->service->parseMany($ar_words);
        $this->assertCount(3, $ar_word_cards);
        $this->assertEquals(mb_strtolower($ar_words[0]), $ar_word_cards[0]->getWord());
        $this->assertEquals(mb_strtolower($ar_words[1]), $ar_word_cards[1]->getWord());
        $this->assertEquals(mb_strtolower($ar_words[2]), $ar_word_cards[2]->getWord());
    }

    public function testParseWhenEmptyWord()
    {
        $http_client = $this->prepareHttpClient(
            200,
            [
                (new ServiceResponseBodyBuilder())
                    ->setError("Can't create wordcard with empty Word")
                    ->build(),
            ]);

        $this->service = new GoMicroserviceParser($http_client);

        $this->expectException(DomainException::class);
        $this->service->parse('');
    }

    public function testParseWhenNotFound()
    {
        $http_client = $this->prepareHttpClient(
            200,
            [
                (new ServiceResponseBodyBuilder())
                    ->setWord("as23dfzdsfds")
                    ->setError("not found")
                    ->build(),
            ]);

        $this->service = new GoMicroserviceParser($http_client);
        $this->expectException(DomainException::class);
        $this->service->parse('as23dfzdsfds');
    }

    private function prepareHttpClient(int $code, array $body_to_json): HttpClient
    {
        $body_string = json_encode($body_to_json);
        $response = $this->makeResponse($code, $body_string);
        return $this->makeHttpClientWithResponse($response);
    }

    private function makeResponse(int $code, string $body_string = ''): ResponseInterface
    {
        $body = $this->createMock(StreamInterface::class);
        $body->method('getContents')
            ->willReturn($body_string);
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getStatusCode')
            ->willReturn($code);
        $response->method('getBody')
            ->willReturn($body);

        return $response;
    }

    private function makeHttpClientWithResponse(ResponseInterface $response): HttpClient
    {
        $http_client = $this->createMock(HttpClient::class);
        $http_client->method('get')
            ->willReturn($response);
        return $http_client;
    }
}

<?php

namespace App\UseCases\English\Tests\Unit\Services\Parser;

class ServiceResponseBodyBuilder
{
    private string $word = '';
    private string $error = '';
    private string $transcription = '';
    /**
     * @var string[]
     */
    private array $translate_noun = [];
    /**
     * @var string[]
     */
    private array $translate_verb = [];
    /**
     * @var string[]
     */
    private array $translate_adjective = [];
    private string $example = '';
    private string $audio_download_url = '';

    public function build(): object
    {
        return (object)[
            'word' => $this->word,
            'error' => $this->error,
            'word_card' => (object)[
                "word" => mb_strtolower($this->word),
                "transcription" => $this->transcription,
                "translate_noun" => !empty($this->translate_noun) ? $this->translate_noun : null,
                "translate_verb" => !empty($this->translate_verb) ? $this->translate_verb : null,
                "translate_adjective" => !empty($this->translate_adjective) ? $this->translate_adjective : null,
                "example" => $this->example,
                "audio_download_url" => $this->audio_download_url,
            ],
        ];
    }

    public function setWord(string $word): self
    {
        $this->word = $word;
        return $this;
    }

    public function setError(string $error): self
    {
        $this->error = $error;
        return $this;
    }

    public function setTranscription(string $transcription): self
    {
        $this->transcription = $transcription;
        return $this;
    }

    public function setExample(string $example): self
    {
        $this->example = $example;
        return $this;
    }

    public function setAudioDownloadUrl(string $audio_download_url): self
    {
        $this->audio_download_url = $audio_download_url;
        return $this;
    }

    /**
     * @param string $translate_noun
     * @return ServiceResponseBodyBuilder
     */
    public function setTranslateNoun(string $translate_noun): ServiceResponseBodyBuilder
    {
        $this->translate_noun[] = $translate_noun;
        return $this;
    }

    /**
     * @param string $translate_verb
     * @return ServiceResponseBodyBuilder
     */
    public function setTranslateVerb(string $translate_verb): ServiceResponseBodyBuilder
    {
        $this->translate_verb[] = $translate_verb;
        return $this;
    }

    /**
     * @param string $translate_adjective
     * @return ServiceResponseBodyBuilder
     */
    public function setTranslateAdjective(string $translate_adjective): ServiceResponseBodyBuilder
    {
        $this->translate_adjective[] = $translate_adjective;
        return $this;
    }
}

<?php

namespace App\UseCases\English\Tests\Unit\Services\Export;

use App\UseCases\English\Services\Export\EnglishCardExporter;
use App\UseCases\English\Tests\TestCase;

class EnglishCardExporterTest extends TestCase
{
    private EnglishCardExporter $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new EnglishCardExporter;
    }

    public function testExport()
    {
        $tag = 'tag';
        $word_card_1 = $this->createWordCard();
        $word_card_2 = $this->createWordCard();
        $ar_words = [
            $word_card_1,
            $word_card_2
        ];

        $export_text = $this->service->renderExportFile($ar_words, $tag);

        $this->assertStringContainsString($word_card_1->getWord(), $export_text);
        $this->assertStringContainsString($word_card_1->getTranscription(), $export_text);
        $this->assertStringContainsString($word_card_1->getTranslateNoun()[0], $export_text);
        $this->assertStringContainsString($word_card_1->getTranslateVerb()[0], $export_text);
        $this->assertStringContainsString($word_card_1->getExample(), $export_text);

        $this->assertStringContainsString($word_card_2->getWord(), $export_text);
        $this->assertStringContainsString($word_card_2->getTranscription(), $export_text);
        $this->assertStringContainsString($word_card_2->getTranslateNoun()[0], $export_text);
        $this->assertStringContainsString($word_card_2->getTranslateVerb()[0], $export_text);
        $this->assertStringContainsString($word_card_2->getExample(), $export_text);
    }
}

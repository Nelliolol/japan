<?php

namespace App\UseCases\English\Commands\GenerateAnkiExportFile;

use App\Services\FileHelper\DTO\DownloadTask;
use App\Services\FileHelper\FileHelper;
use App\Services\UUIDGenerator\UUIDGenerator;
use App\useCases\English\Entities\WordCard;
use App\UseCases\English\Services\Export\Exporter;
use App\UseCases\English\Services\Parser\Parser;
use DomainException;

class Handler
{
    private Parser $word_parser;
    private Exporter $exporter;
    private FileHelper $file_helper;
    private UUIDGenerator $UUID_generator;

    /**
     * @param Parser $word_parser
     * @param Exporter $exporter
     * @param FileHelper $file_helper
     * @param UUIDGenerator $UUID_generator
     */
    public function __construct(Parser $word_parser, Exporter $exporter, FileHelper $file_helper, UUIDGenerator $UUID_generator)
    {
        $this->word_parser = $word_parser;
        $this->exporter = $exporter;
        $this->file_helper = $file_helper;
        $this->UUID_generator = $UUID_generator;
    }

    public function handle(Command $command): Response
    {
        $ar_word_cards = $this->getWordsToExport($command->getArWordsToExport());
        if ($command->isEnabledDownloadingAudioFiles()) {
            $this->downloadAudioFiles($ar_word_cards, $command->getPathToAudioDirectory());
        }
        $this->generateExportFile($ar_word_cards, $command->getTag(), $command->getPathToExportFile());
        return $this->makeResponse($command->getArWordsToExport(), $ar_word_cards);
    }

    /**
     * @param array $ar_parsed_words
     * @return WordCard[]
     */
    private function getWordsToExport(array $ar_parsed_words): array
    {
        $ar_parsed_words = $this->word_parser->parseMany($ar_parsed_words);
        if (empty($ar_parsed_words)) {
            throw new DomainException("No data for export");
        }
        return $ar_parsed_words;
    }

    /**
     * @param WordCard[] $ar_word_cards
     * @param string $tag
     * @param string $path_to_export_file
     * @return void
     */
    private function generateExportFile(array $ar_word_cards, string $tag, string $path_to_export_file): void
    {
        $export_text = $this->exporter->renderExportFile($ar_word_cards, $tag);
        $this->file_helper->put($path_to_export_file, $export_text);
    }

    /**
     * @param WordCard[] $ar_word_cards
     * @param string $path_to_directory
     * @return void
     */
    private function downloadAudioFiles(array $ar_word_cards, string $path_to_directory): void
    {
        $ar_tasks = $this->getArDownloadTasks($ar_word_cards, $path_to_directory);
        if (empty($ar_tasks)) {
            return;
        }

        $this->file_helper->deleteDirectory($path_to_directory);
        $this->file_helper->downloadFiles($ar_tasks);
        $this->attachDownloadedAudioToWords($ar_word_cards, $ar_tasks);
    }


    /**
     * @param WordCard[] $ar_word_cards
     * @param string $path_to_directory
     * @return DownloadTask[]
     */
    private function getArDownloadTasks(array $ar_word_cards, string $path_to_directory): array
    {
        $ar_tasks = [];

        foreach ($ar_word_cards as $word_card) {
            if ($word_card->getAudioDownloadUrl()) {
                $ar_tasks[] = new DownloadTask(
                    $word_card->getAudioDownloadUrl(),
                    $this->getPathToSaveAudioFile($path_to_directory)
                );
            }
        }

        return $ar_tasks;
    }

    /**
     * @param string $path_to_directory
     * @return string
     */
    private function getPathToSaveAudioFile(string $path_to_directory): string
    {
        return $path_to_directory . $this->UUID_generator->generate() . '.mp3';
    }

    /**
     * @param WordCard[] $ar_word_cards
     * @param DownloadTask[] $ar_tasks
     * @return void
     */
    private function attachDownloadedAudioToWords(array $ar_word_cards, array $ar_tasks): void
    {
        array_map(function ($task) use ($ar_word_cards) {
            if ($this->file_helper->exists($task->path_to_save_file)) {
                foreach ($ar_word_cards as $word_card) {
                    if ($word_card->getAudioDownloadUrl() == $task->url) {
                        $word_card->setDownloadedAudioFileName(basename($task->path_to_save_file));
                    }
                }
            }
        }, $ar_tasks);
    }

    /**
     * @param string[] $ar_words_to_export
     * @param WordCard[] $parsed_word_card
     */
    private function makeResponse(array $ar_words_to_export, array $parsed_word_card): Response
    {
        return new Response($ar_words_to_export, $parsed_word_card);
    }
}

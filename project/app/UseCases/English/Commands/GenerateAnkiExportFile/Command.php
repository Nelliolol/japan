<?php

namespace App\UseCases\English\Commands\GenerateAnkiExportFile;

use Webmozart\Assert\Assert;

class Command
{
    /**
     * @var string[]
     */
    private array $ar_words_to_export;
    private string $tag;
    private string $path_to_export_file;
    private string $path_to_audio_directory;
    private bool $download_audio = false;
    private bool $debug = false;

    /**
     * @param string $tag
     * @param string[] $ar_words_to_export
     * @param string $path_to_export_file
     * @param string $path_to_audio_directory
     */
    public function __construct(
        string $tag,
        array $ar_words_to_export,
        string $path_to_export_file,
        string $path_to_audio_directory
    )
    {
        Assert::notEmpty($tag);
        $this->tag = $tag;

        Assert::notEmpty($ar_words_to_export);
        $this->ar_words_to_export = $ar_words_to_export;

        Assert::notEmpty($path_to_export_file);
        $this->path_to_export_file = $path_to_export_file;

        Assert::notEmpty($path_to_audio_directory);
        $this->path_to_audio_directory = $path_to_audio_directory;
    }

    /**
     * @return array
     */
    public function getArWordsToExport(): array
    {
        return $this->ar_words_to_export;
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @return string
     */
    public function getPathToExportFile(): string
    {
        return $this->path_to_export_file;
    }

    /**
     * @return string
     */
    public function getPathToAudioDirectory(): string
    {
        return $this->path_to_audio_directory;
    }

    public function enableDownloadingAudioFiles(): void
    {
        $this->download_audio = true;
    }

    public function disableDownloadingAudioFiles(): void
    {
        $this->download_audio = false;
    }

    public function isEnabledDownloadingAudioFiles(): bool
    {
        return $this->download_audio;
    }

    public function isEnabledDebug(): bool
    {
        return $this->debug;
    }

    public function enableDebugging(): void
    {
        $this->debug = true;
    }

    public function disableDebugging(): void
    {
        $this->debug = false;
    }
}

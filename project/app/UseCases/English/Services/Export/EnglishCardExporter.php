<?php

namespace App\UseCases\English\Services\Export;

use App\UseCases\English\Entities\WordCard;

class EnglishCardExporter implements Exporter
{
    /**
     * @param WordCard[] $arExportData
     * @param string $tag
     * @return string
     */
    public function renderExportFile(array $arExportData, string $tag ):string
    {
        $separator = ';';
    	return view('exporters.anki_english', compact('arExportData', 'tag', 'separator'))->render();
    }
}

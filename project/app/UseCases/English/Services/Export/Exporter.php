<?php

namespace App\UseCases\English\Services\Export;

use App\UseCases\English\Entities\WordCard;

interface Exporter
{
    /**
     * @param WordCard[] $arExportData
     * @param string $tag
     * @return string
     */
    public function renderExportFile(array $arExportData, string $tag ):string;
}

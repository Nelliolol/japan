<?php

namespace App\UseCases\English\Services\Parser;

use App\Services\HttpClient\HttpClient;
use App\UseCases\English\Entities\WordCard;
use DomainException;

class GoMicroserviceParser implements Parser
{
    private HttpClient $http_client;

    public function __construct( HttpClient $http_client )
    {
        $this->http_client = $http_client;
    }

    public function parse(string $word): WordCard
    {
        $response = $this->getServiceResponse([$word]);

        $response_word = $response[0];
        if ( !empty($response_word['error']) ){
            throw new DomainException($response_word['error']);
        }
        if ( empty($response_word['word_card']) ){
            throw new DomainException("empty word card");
        }

        return $this->createWordCard($response_word['word_card']);
    }

    /**
     * @inheritDoc
     */
    public function parseMany(array $ar_words): array
    {
        $result = [];
        $response = $this->getServiceResponse($ar_words);
        foreach ( $response as $response_word ) {
            if ( !empty($response_word['error']) || empty($response_word['word_card']) ){
                continue;
            }
            $result[] = $this->createWordCard($response_word['word_card']);
        }
        return $result;
    }

    private function getServiceResponse(array $ar_words): array
    {
        $url = $this->getServiceUrl($ar_words);
        $result = $this->http_client->get($url);

        if ($result->getStatusCode() != 200) {
            throw new DomainException('Error request. Code ' . $result->getStatusCode());
        }

        $response =  json_decode($result->getBody()->getContents(), true);

        if (count($response) < 1){
            throw new DomainException('Empty response');
        }

        return $response;
    }

    private function getServiceUrl( array $ar_words ): string {
        $url = env("ENGLISH_WORD_PARSER_URL") . "/?" . http_build_query([
            'words' => $ar_words
        ]);
        return preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $url);
    }

    /**
     * @param $word_card_response
     * @return WordCard
     */
    public function createWordCard($word_card_response): WordCard
    {
        $word_card = new WordCard($word_card_response['word']);
        if (!empty($word_card_response['transcription'])) {
            $word_card->setTranscription($word_card_response['transcription']);
        }
        if (!empty($word_card_response['example'])) {
            $word_card->setExample($word_card_response['example']);
        }
        if (!empty($word_card_response['audio_download_url'])) {
            $word_card->addAudioDownloadUrl($word_card_response['audio_download_url']);
        }
        foreach ($word_card_response['translate_noun'] ?? [] as $translate) {
            $word_card->addTranslateNoun($translate);
        }
        foreach ($word_card_response['translate_verb'] ?? [] as $translate) {
            $word_card->addTranslateVerb($translate);
        }
        foreach ($word_card_response['translate_adjective'] ?? [] as $translate) {
            $word_card->addTranslateAdjective($translate);
        }
        return $word_card;
    }
}

<?php

namespace App\UseCases\English\Services\Parser;

use App\useCases\English\Entities\WordCard;

interface Parser
{
    public function parse(string $word): WordCard;

    /**
     * @param string[] $ar_words
     * @return WordCard[]
     */
    public function parseMany(array $ar_words): array;
}

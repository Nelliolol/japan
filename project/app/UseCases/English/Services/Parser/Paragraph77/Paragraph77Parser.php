<?php

namespace App\UseCases\English\Services\Parser\Paragraph77;

use App\Services\HttpClient\HttpClient;
use App\UseCases\English\Entities\WordCard;
use App\UseCases\English\Services\Parser\Parser;
use DomainException;
use Exception;
use KubAT\PhpSimple\HtmlDomParser;

class Paragraph77Parser implements Parser
{
    private HttpClient $http_client;

    public function __construct( HttpClient $http_client )
    {
        $this->http_client = $http_client;
    }

    /**
     * @param string[] $ar_words
     * @return WordCard[]
     */
    public function parseMany(array $ar_words): array
    {
        $result = [];
        foreach ( $ar_words as $word ){
            try {
                $result[] = $this->parse($word);
            } catch ( Exception $exception){
                continue;
            }
        }

        return $result;
    }

    public function parse(string $word): WordCard
    {
        if (strlen($word) < 1) {
            throw new DomainException("Word is null");
        }

        $word = mb_strtolower($word);
        $word_card = (new WordCard($word));

        $response = $this->getServiceResponse($word_card->getWord());

        $dom = HtmlDomParser::str_get_html($response);

    	if ( !$dom->find('h1') ){
    		throw new DomainException("Not found");
    	}

        foreach ($dom->find('span.transcription') ?? [] as $transcription) {
            $word_card->setTranscription($transcription->plaintext);
            break;
        }

        foreach ($dom->find('#wd_content .block .ex_o') ?? [] as $example) {
            $word_card->setExample($example->plaintext);
            break;
        }

        foreach ($dom->find('#wd_content .pos_item') ?? [] as $h4) {

            $attr = $h4->plaintext;

            $function_name = null;

            if ($this->isNoun($attr)) {
                $function_name = 'addTranslateNoun';
            } else if ($this->isVerb($attr)) {
                $function_name = 'addTranslateVerb';
            } else if ($this->isAdjective($attr)) {
                $function_name = 'addTranslateAdjective';
            } else if ($this->isAdverb($attr)) {
                $function_name = 'addTranslateAdjective';
            } else if ($this->isPos($attr)) {
                $function_name = 'addTranslateAdjective';
            }

            if ($function_name) {

                // remove examples
                $html_with_translate = $h4->next_sibling();
                foreach ($html_with_translate->children() as $child_node) {
                    if (strpos($child_node->class, 'ex') !== false) {
                        $child_node->outertext = '';
                    }
                }

                $translate = $html_with_translate->innertext;

                if (strpos($translate, '<br/>') !== false) {
                    foreach (explode('<br/>', $translate) as $translate_part) {
                        if ($translate_part) {
                            $word_card->$function_name($translate_part);
                        }
                    }

                } else {
                    $word_card->$function_name($h4->next_sibling()->plaintext);
                }
            }
        }

        foreach ($dom->find('.light_tr') ?? [] as $translate) {
            $word_card->addTranslateVerb($translate->plaintext);
            break;
        }

        foreach ($dom->find('audio source') ?? [] as $source) {

            $word_card->addAudioDownloadUrl('https://wooordhunt.ru' . $source->getAttribute('src'));
            break;
        }

        return $word_card;
    }

    public function getServiceResponse(string $word)
    {
        $url = 'https://wooordhunt.ru/word/' . $word;

        $result = $this->http_client->get($url);

        if ($result->getStatusCode() != 200) {
            throw new DomainException('Error request. Code ' . $result->getStatusCode());
        }

        return $result->getBody()->getContents();
    }

    private function isNoun(string $attr_value): bool
    {
        return mb_strpos($attr_value, 'существительное') !== false;
    }

    private function isVerb(string $attr_value): bool
    {
        return mb_strpos($attr_value, 'глагол') !== false;
    }

    private function isAdjective(string $attr_value): bool
    {
        return mb_strpos($attr_value, 'прилагательное') !== false;
    }

    private function isAdverb(string $attr_value): bool
    {
        return mb_strpos($attr_value, 'наречие') !== false;
    }

    private function isPos(string $attr_value): bool
    {
        return mb_strpos($attr_value, 'предлог') !== false;
    }
}

<?php

namespace App\UseCases\English\Entities;

class WordCard
{
    const MAX_TRANSLATIONS_COUNT = 5;

    private WordCardField $word;
    private ?WordCardField $transcription = null;
    /**
     * @var WordCardField[]
     */
    private array $translate_noun = [];
    /**
     * @var WordCardField[]
     */
    private array $translate_verb = [];
    /**
     * @var WordCardField[]
     */
    private array $translate_adjective = [];
    private ?WordCardField $example = null;
    private string $audio_download_url = '';
    private string $downloaded_audio_file_name = '';

    public function __construct(string $word)
    {
        $this->word = new WordCardField($word);
    }

    /**
     * @return string
     */
    public function getWord(): string
    {
        return $this->word->getValue();
    }

    /**
     * @return ?string
     */
    public function getTranscription(): ?string
    {
        return $this->transcription ? $this->transcription->getValue() : null;
    }

    public function setTranscription(string $transcription): WordCard
    {
        $this->transcription = new WordCardField($transcription);

        return $this;
    }

    /**
     * @return string[]
     */
    public function getTranslateNoun(): array
    {
        return array_map(fn($value)=>$value->getValue(), $this->translate_noun);
    }

    public function addTranslateNoun(string $translate_noun): WordCard
    {
        if ($this->canAddMoreTranslations($this->getTranslateNoun())) {
            $this->translate_noun[] = new WordCardField($translate_noun);
        }

        return $this;
    }

    /**
     * @return string[]
     */
    public function getTranslateVerb(): array
    {
        return array_map(fn($value)=>$value->getValue(), $this->translate_verb);
    }

    public function addTranslateVerb(string $translate_verb): WordCard
    {
        if ($this->canAddMoreTranslations($this->getTranslateVerb())) {
            $this->translate_verb[] = new WordCardField($translate_verb);
        }

        return $this;
    }

    /**
     * @return string[]
     */
    public function getTranslateAdjective(): array
    {
        return array_map(fn($value)=>$value->getValue(), $this->translate_adjective);
    }

    public function addTranslateAdjective(string $translate_adjective): WordCard
    {
        if ($this->canAddMoreTranslations($this->getTranslateAdjective())) {
            $this->translate_adjective[] = new WordCardField($translate_adjective);
        }

        return $this;
    }

    /**
     * @return ?string
     */
    public function getExample(): ?string
    {
        return $this->example ? $this->example->getValue() : null;
    }

    public function setExample(string $example): WordCard
    {
        $this->example = new WordCardField($example);
        return $this;
    }

    public function addAudioDownloadUrl(string $audio): WordCard
    {
        $this->audio_download_url = $audio;
        return $this;
    }

    public function getAudioDownloadUrl(): string
    {
        return $this->audio_download_url;
    }

    /**
     * @param string $file_name
     * @return self
     */
    public function setDownloadedAudioFileName(string $file_name): WordCard
    {
        $this->downloaded_audio_file_name = $file_name;
        return $this;
    }

    public function getDownloadedAudioFileName(): string
    {
        return $this->downloaded_audio_file_name;
    }

    private function canAddMoreTranslations(array $already_added_words): bool
    {
        if (count($already_added_words) >= self::MAX_TRANSLATIONS_COUNT) {
            return false;
        }

        return true;
    }
}

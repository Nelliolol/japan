<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/find/{search_kanji}', 'HomeController@find')->name('find');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});

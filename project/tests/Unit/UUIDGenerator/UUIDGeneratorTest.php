<?php

namespace App\UseCases\English\Tests\Unit\Services\Parser\UUIDGenerator;

use App\Services\UUIDGenerator\LaravelStringUUIDGenerator;
use Tests\TestCase;

class UUIDGeneratorTest extends TestCase
{
    private LaravelStringUUIDGenerator $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new LaravelStringUUIDGenerator;
    }

    public function testGenerate()
    {
        $uuid = $this->service->generate();
        $uuid2 = $this->service->generate();

        $this->assertNotEmpty($uuid);
        $this->assertNotEmpty($uuid2);
        $this->assertNotEquals($uuid, $uuid2);
    }
}

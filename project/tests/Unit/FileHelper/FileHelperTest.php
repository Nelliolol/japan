<?php

namespace Tests\Unit\FileHelper;

use App\Services\FileHelper\DTO\DownloadTask;
use App\Services\FileHelper\LaravelStorageFileHelper;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class FileHelperTest extends TestCase
{
    private LaravelStorageFileHelper $service;
    private string $storage_path = 'tests/';

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new LaravelStorageFileHelper;
        $this->cleanTestFilesDirectory();
    }

    /**
     * @throws \Throwable
     */
    protected function tearDown(): void
    {
        $this->cleanTestFilesDirectory();
        parent::tearDown();
    }

    public function testPutFile()
    {
        $file_path = Storage::path($this->storage_path . $this->faker->uuid . '.' . $this->faker->fileExtension);
        $content = '1';
        $this->assertFileDoesNotExist($file_path);
        $this->service->put($file_path, $content);
        $this->assertFileExists($file_path);
    }

    public function testDeleteDirectory()
    {
        $file_path = Storage::path($this->storage_path . $this->faker->uuid . '.' . $this->faker->fileExtension);
        $content = '1';
        $this->service->put($file_path, $content);

        $directory_path = Storage::path($this->storage_path);
        $this->directoryExists($directory_path);
        $this->service->deleteDirectory($directory_path);
        $this->assertDirectoryDoesNotExist($directory_path);
    }

    public function testDownloadFiles()
    {
        $url = 'https://www.php.net/images/logos/php-logo.svg';
        $path_to_save_directory = Storage::path($this->storage_path);
        $path_to_save_file_1 = $path_to_save_directory . '1.txt';
        $path_to_save_file_2 = $path_to_save_directory . '2.txt';
        $task_1 = new DownloadTask($url, $path_to_save_file_1);
        $task_2 = new DownloadTask($url, $path_to_save_file_2);
        $ar_tasks = [
            $task_1,
            $task_2,
        ];

        $this->service->downloadFiles($ar_tasks);
        $this->assertFileExists($path_to_save_file_1);
        $this->assertFileExists($path_to_save_file_2);
    }

    public function testDownloadFilesWhenBadUrl()
    {
        $url_1 = 'https://www.google.com//images/branding/googlelogo/2x/googlelogo_light_color_92x30dp.pngasd';
        $url_2 = 'https://www.php.net/images/logos/php-logo.svg';
        $path_to_save_directory = Storage::path($this->storage_path);
        $path_to_save_file_1 = $path_to_save_directory . '1.txt';
        $path_to_save_file_2 = $path_to_save_directory . '2.txt';
        $task_1 = new DownloadTask($url_1, $path_to_save_file_1);
        $task_2 = new DownloadTask($url_2, $path_to_save_file_2);
        $ar_tasks = [
            $task_1,
            $task_2
        ];

        $this->service->downloadFiles($ar_tasks);
        $this->assertFileDoesNotExist($path_to_save_file_1);
        $this->assertFileExists($path_to_save_file_2);
    }

    public function testExistWhenFileExist()
    {
        $path = Storage::path($this->storage_path . $this->faker->uuid . '.' . $this->faker->fileExtension);
        $content = '1';
        $this->service->put($path, $content);
        $this->assertTrue($this->service->exists($path));
    }

    public function testExistWhenFileNotExist()
    {
        $path = Storage::path($this->storage_path . $this->faker->uuid . '.' . $this->faker->fileExtension);
        $this->assertFalse($this->service->exists($path));
    }

    private function cleanTestFilesDirectory(): void
    {
        Storage::deleteDirectory($this->storage_path);
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;

class FindKanjiInformationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFindKanjiInfo()
    {
        $response = $this->get('/find/食べる');
        $responseText = $response->getContent();

        $response->assertStatus(200);
        $yarxiText = 'еда, пища приём пищи';
        $jishoText = 'to eat';
        $sentenceText = '食べな';

        $this->assertStringContainsStringIgnoringCase( $yarxiText, $responseText );
        $this->assertStringContainsStringIgnoringCase( $jishoText, $responseText );
        $this->assertStringContainsStringIgnoringCase( $sentenceText, $responseText );
    }
}

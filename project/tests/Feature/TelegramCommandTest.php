<?php

namespace Tests\Feature;

use App\Services\Telegram\TelegramClient;
use App\Services\Telegram\TelegramHelper;
use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Handler;
use App\UseCases\Japanese\Commands\GenerateAnkiExportFile\Response;
use App\UseCases\Japanese\Entities\WordCard;
use DomainException;
use Exception;
use Mockery\MockInterface;
use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class TelegramCommandTest extends TestCase
{
    public function testProperRequest()
    {
        $mock_anki_generator = $this->mock(Handler::class, function (MockInterface $mock) {
            $response = new Response(['word1', 'word2'], [
                new WordCard('word1')
            ]);
            $mock->shouldReceive('handle')
                ->once()
                ->andReturn($response);
        });

        $telegram_client = $this->mock(TelegramClient::class, function (MockInterface $mock) {
            $mock->shouldReceive('sendMessage')
                ->once();
            $mock->shouldReceive('sendDocument')
                ->once();
        });

        Storage::shouldReceive('delete')
            ->once();

        Storage::shouldReceive('path')
            ->once()
            ->andReturn('test.txt');

        Log::shouldReceive('error')
            ->never();

        $data = $this->getRequestData();
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);

        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testCommandWithUnsearchableContentRequest()
    {
        $mock_anki_generator = $this->mock(Handler::class, function (MockInterface $mock) {
            $mock->shouldReceive('handle')
                ->once()
                ->andThrow(new DomainException("No data for export"));
        });

        $telegram_client = $this->mock(TelegramClient::class, function (MockInterface $mock) {
            $mock->shouldReceive('sendMessage')
                ->once();
            $mock->shouldReceive('sendDocument')
                ->never();
        });

        Storage::shouldReceive('delete')
            ->once();

        Storage::shouldReceive('path')
            ->once()
            ->andReturn('test.txt');

        Log::shouldReceive('error')
            ->never();

        $data = $this->getRequestData('/generate 1 2 3');
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);

        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testUnexpectedError()
    {
        $mock_anki_generator = $this->mock(Handler::class, function (MockInterface $mock) {
            $mock->shouldReceive('handle')
                ->once()
                ->andThrow(new Exception("Big error"));
        });

        $telegram_client = $this->mock(TelegramClient::class, function (MockInterface $mock) {
            $mock->shouldReceive('sendMessage')
                ->never();
            $mock->shouldReceive('sendDocument')
                ->never();
        });

        Storage::shouldReceive('delete')
            ->once();

        Storage::shouldReceive('path')
            ->once()
            ->andReturn('test.txt');

        Log::shouldReceive('error')
            ->once();

        $data = $this->getRequestData('/generate 1 2 3');
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);

        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testRequestWithOutDocument()
    {
        $mock_anki_generator = $this->mock(Handler::class, function (MockInterface $mock) {
            $response = new Response(['word1', 'word2'], []);
            $mock->shouldReceive('handle')
                ->once()
                ->andReturn($response);
        });

        $telegram_client = $this->mock(TelegramClient::class, function (MockInterface $mock) {
            $mock->shouldReceive('sendMessage')
                ->once();
            $mock->shouldReceive('sendDocument')
                ->never();
        });

        $data = $this->getRequestData();
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);

        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testWithWrongHeader()
    {
        $this->mockServices();

        $data = $this->getRequestData();
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => 'wrong header',
        ])->post(route('telegram'), $data);
        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testWithOutHeader()
    {
        $this->mockServices();

        $data = $this->getRequestData();
        $response = $this->post(route('telegram'), $data);
        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testWithOtherCommand()
    {
        $this->mockServices();

        $data = $this->getRequestData('/test 1 2 3');
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);
        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testWithWrongCommandFormat()
    {
        $this->mockServices();

        $data = $this->getRequestData('/generate');
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);
        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testWithWrongCommandFormat2()
    {
        $this->mockServices();

        $data = $this->getRequestData('/generate ');
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);
        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testWithWrongCommandFormat3()
    {
        $this->mockServices();

        $data = $this->getRequestData('/generate  ');
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);
        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testWhenNotExistChatID()
    {
        $this->mockServices();

        $data = $this->getRequestData();
        unset($data['message']['chat']['id']);
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);
        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    public function testWhenNotExistMessageID()
    {
        $this->mockServices();

        $data = $this->getRequestData();
        unset($data['message']['message_id']);
        $response = $this->withHeaders([
            TelegramHelper::TELEGRAM_HEADER => env('TELEGRAM_RESPONSE_TOKEN'),
        ])->post(route('telegram'), $data);
        $this->assertEmpty($response->content());
        $response->assertStatus(200);
    }

    private function getRequestData(string $message = ''): array
    {
        if (strlen($message) < 1) {
            $message = TelegramHelper::getCommandPrefix() . '食べる 飲む';
        }

        return [
            'update_id' => 362022313,
            'message' => [
                'message_id' => 59,
                'from' => [
                    'id' => 764084925,
                    'is_bot' => false,
                    'first_name' => 'Max',
                    'last_name' => 'Istomin',
                    'username' => 'nelliolol',
                    'language_code' => 'en',
                ],
                'chat' => [
                    'id' => -945622292,
                    'title' => 'Anki helper group 🖥',
                    'type' => 'group',
                    'all_members_are_administrators' => true,
                ],
                'date' => 1679224235,
                'text' => $message,
                'entities' => [
                    [
                        'offset' => 0,
                        'length' => 9,
                        'type' => 'bot_command',
                    ],
                ],
            ],
        ];
    }

    private function mockServices(): void
    {
        $mock_anki_generator = $this->mock(Handler::class, function (MockInterface $mock) {
            $mock->shouldReceive('handle')
                ->never();
        });

        $telegram_client = $this->mock(TelegramClient::class, function (MockInterface $mock) {
            $mock->shouldReceive('sendMessage')
                ->never();
        });
    }
}

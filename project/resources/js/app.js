require('./bootstrap');

$(function () {
    var form = $('#search');

    form.submit(function (e) {
        e.preventDefault();

        let inputVal = $('#search').find('input[type=text]').val();

        if (inputVal) {

            let url = form.attr('action') + inputVal;
            window.location = url;
        }
    })

    $('#refresh').click(function () {
        location.reload();
    });
});

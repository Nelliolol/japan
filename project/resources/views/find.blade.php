@extends('layouts.app')

@section('content')

    @include('partials.search', ['search_kanji' => $search_kanji])

    @foreach ($result->getArKanjiParseResults() as $response)
	    <div class="my-3 p-3 bg-white rounded box-shadow">
	        <h6 class="border-bottom border-gray pb-2 mb-0 h4">{{ $response->getParserName() }}</h6>

	        @if ( $response->isError() )
	        	<div class="alert alert-danger" role="alert">
				  {{ $response->getError() }}
				</div>
			@else
	        	<div class="pt-3">
	        		@include('parcers.jisho', ['data' => $response->getKanji()])
	        	</div>
			@endif
	    </div>
	@endforeach


	@if ( $result->getParsedSentences()->getSentence() )
		<div class="my-3 p-3 bg-white rounded box-shadow">
			<h6 class="border-bottom border-gray pb-2 mb-0 h4">Sentences</h6>
			<div class="pt-3">
		    	@foreach ($result->getParsedSentences()->getSentence() as $sentence)
					<blockquote class="blockquote">
					  <p class="mb-0">{{ $sentence->getJapText() }}</p>
					  <footer class="blockquote-footer">{{ $sentence->getEngText() }}</footer>
					</blockquote>
				@endforeach
			</div>
		</div>
	@endif
@endsection

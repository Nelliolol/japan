<div class="container">
	<h1>Enter the kanji</h1>
	<form id="search" action="/find/" >
		<div class="row">
		    <div class="col-md-10 mb-2">
		      <input autofocus type="text" class="form-control form-control-lg" value="{{$search_kanji}}" placeholder="Hiragana or Kanji">
		    </div>
		    <div class="col-md-2">
		      <button class="btn btn-primary btn-lg btn-block" type="submit">search</button>
		    </div>
		</div>
	</form>
</div>

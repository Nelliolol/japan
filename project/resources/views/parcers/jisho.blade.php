<div class="container">
	<div class="row mb-2">
		@foreach ($data as $kanji)

			<div class="col-sm-12 mb-2" >
				<div class="media ">
					<div class="align-self-center mr-3">
						<div class="h1 mb-0">
							<span class="text-dark" >{{ $kanji->getName() }}</span>
						</div>
					</div>
					<div class="media-body">
						{{-- 
							<h5 class="mt-0">Center-aligned media</h5>
						--}}
						@if ( $kanji->getKun() )
							<div class="text-muted">Kun</div>
							<div class="mb-3">
								@foreach ($kanji->getKun() as $kun)
									<p class="card-text text-success mb-auto h5">{{ $kun }}</p>
								@endforeach
							</div>
						@endif
						@if ( $kanji->getON() )
							<div class="text-muted">On</div>
							<div class="mb-3">
								@foreach ($kanji->getON() as $kun)
									<p class="card-text mb-auto text-primary h5">{{ $kun }}</p>
								@endforeach
							</div>
						@endif
						@if ( $kanji->getReading() )
							<div class="text-muted">Hiragana</div>
							<p class="card-text mb-3 h5">
								{{ $kanji->getReading() }}
							</p>
						@endif
						@if ( $kanji->getTranslate() )
							<div class="text-muted">Translate</div>
							<p class="card-text mb-3 h5">
								@foreach ($kanji->getTranslate() as $translate)
									{!! $translate !!}
									<br>
								@endforeach
							</p>
						@endif
					</div>
				</div>
			</div>

		@endforeach
	</div>
</div>
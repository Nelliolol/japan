@extends('layouts.app')

@section('title', 'Kanji training')

@section('content')
    @include('partials.search', ['search_kanji' => null])

	<div class="container mt-5">

		<p class="h1">サービスの内容 from Kube </p>

		<ul>
			<li>
				<p><kbd>PHP + Laravel</kbd>, <kbd>Mysql</kbd>で作った、<b>翻訳</b>ために<a href="{{ route('find', ['search_kanji' => '翻訳']) }}">アプリです</a>。　インターネットで二つウエブサイトがあります。一つのはひらがなと英語の翻訳があります。二つのはロシアの翻訳があります。私は漢字を勉強しているの時両方使っているから、ちっと不便なことがあります。時間を守るし便利に使うためにそのプロジェクトを作りました。私のウエブサイトは他のウエブサイトからデイターを保存して便利に見せます。</p>
			</li>
		</ul>
	</div>

@endsection

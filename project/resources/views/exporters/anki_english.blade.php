#separator:{!! $separator !!}
#tags:{!! $tag !!}
@foreach ($arExportData as $data)
{!! $data->getWord() !!}{!! $separator !!} {!! $data->getTranscription() !!}{!! $separator !!} {!! implode(', ', $data->getTranslateNoun() ) !!}{!! $separator !!} {!! implode(', ', $data->getTranslateVerb()) !!}{!! $separator !!} {!! implode(', ', $data->getTranslateAdjective() ) !!}{!! $separator !!} {!! $data->getExample() !!}{!! $separator !!} {!! $data->getDownloadedAudioFileName() ? '[sound:' . $data->getDownloadedAudioFileName() . ']' : '' !!}
@endforeach

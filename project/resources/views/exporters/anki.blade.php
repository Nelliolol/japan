#separator:{!! $separator !!}
#tags:{!! $tag !!}
@foreach ($arExportData as $data)
{{ $data->getKanji() }}{!! $separator !!} {{ $data->getTranslate() }}{!! $separator !!} {{ $data->getReading()}}{!! $separator !!} {{ $data->getExample()}}
@endforeach

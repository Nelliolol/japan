<?php



function format_duration($seconds)
{
    return formatTime(separateSeconds($seconds));
}

function separateSeconds(int $seconds): array
{
    $rest_seconds = $seconds;

    $seconds_in_minute = 60;
    $seconds_in_hour = $seconds_in_minute * 60;
    $seconds_in_day = $seconds_in_hour * 24;
    $seconds_in_year = $seconds_in_day * 365;

    $years = intdiv($rest_seconds, $seconds_in_year);
    if ($years > 0) {
        $rest_seconds -= $years * $seconds_in_year;
    } else {
        $years = 0;
    }

    $days = intdiv($rest_seconds, $seconds_in_day);
    if ($days > 0) {
        $rest_seconds -= $days * $seconds_in_day;
    } else {
        $days = 0;
    }

    $hours = intdiv($rest_seconds, $seconds_in_hour);
    if ($hours > 0) {
        $rest_seconds -= $hours * $seconds_in_hour;
    } else {
        $hours = 0;
    }

//     $minutes = intdiv($rest_seconds, $seconds_in_minute);
//     if ($minutes > 0) {
//         $rest_seconds -= $minutes * $seconds_in_minute;
//     } else {
//         $minutes = 0;
//     }
    $minutes = calculateInterval($rest_seconds, $seconds_in_minute);
    $rest_seconds = subscructSecondsByInterval($rest_seconds, $minutes, $seconds_in_minute);

    return [
        'year' => $years,
        'day' => $days,
        'hour' => $hours,
        'minute' => $minutes,
        'second' => $rest_seconds,
    ];
}

function calculateInterval( int $seconds, int $interval_seconds ): int
{
    $interval = intdiv($seconds, $interval_seconds);
    if ($interval < 0) {
        $interval = 0;
    }

    return $interval;
}

function subscructSecondsByInterval( int $rest_seconds, int $interval, int $interval_seconds): int
{
    if ($interval > 0) {
        return $rest_seconds - $interval * $interval_seconds;
    }
    return $rest_seconds;
}

function formatTime(array $separatedTime): string
{
    $ar_intervals = getIntervalsToShow($separatedTime);
    return formatIntervals($ar_intervals);
}

function getIntervalsToShow(array $separatedTime): array
{
    $ar_showed_time = [];
    foreach ($separatedTime as $intrval => $count) {
        if ($count > 0) {
            $ar_showed_time[] = $count . ' ' . $intrval . ($count > 1 ? 's' : '');
        }
    }
    return $ar_showed_time;
}

function formatIntervals(array $intervals): string
{
    if (empty($intervals)) {
        return 'now';
    }

    $count_intervals = count($intervals);
    if ($count_intervals == 1) {
        return $intervals[0];
    }

    $last = array_pop($intervals);
    return implode(', ', $intervals) . ' and ' . $last;
}




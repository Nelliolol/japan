package parser

import (
	"english-word-parser/entities"
	"errors"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"net/http"
	"strings"
)

const baseUrl = "https://wooordhunt.ru"

type Paragraph77Parser struct {
	htmlDoc *goquery.Document
}

func NewParagraph77Parser() *Paragraph77Parser {
	return &Paragraph77Parser{}
}

func (p *Paragraph77Parser) Parse(word string) (entities.WordCard, error) {

	word = strings.ToLower(word)

	doc, err := getHtmlDocument(word)
	if err != nil {
		return entities.WordCard{}, err
	}
	p.htmlDoc = doc

	return p.createWordCard(word)
}

func getHtmlDocument(word string) (*goquery.Document, error) {

	res, err := getHtmlContent(word)
	if err != nil {
		return &goquery.Document{}, err
	}

	defer func(res io.ReadCloser) {
		err := res.Close()
		if err != nil {

		}
	}(res)

	doc, err := goquery.NewDocumentFromReader(res)
	if err != nil {
		return &goquery.Document{}, err
	}

	return doc, nil
}

func getHtmlContent(word string) (io.ReadCloser, error) {
	res, err := http.Get(getWordPageUrl(word))
	if err != nil {
		return nil, err
	}

	if res.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("status code error: %d %s", res.StatusCode, res.Status))
	}

	return res.Body, nil
}

func getWordPageUrl(word string) string {
	return baseUrl + "/word/" + word
}

func (p *Paragraph77Parser) createWordCard(word string) (entities.WordCard, error) {
	err := p.checkWordPageExist()
	if err != nil {
		return entities.WordCard{}, err
	}

	wordCard, err := entities.NewWordCard(word)
	if err != nil {
		return entities.WordCard{}, err
	}

	example := p.findExample()
	if len(example) > 0 {
		wordCard.SetExample(example)
	}

	transcription := p.findTranscription()
	if len(transcription) > 0 {
		wordCard.SetTranscription(transcription)
	}

	audioDownloadUrl := p.findAudioDownloadUrl()
	if len(audioDownloadUrl) > 0 {
		wordCard.SetAudioDownloadUrl(audioDownloadUrl)
	}

	for _, translate := range p.findTranslateNoun() {
		wordCard.AddTranslateNoun(translate)
	}

	for _, translate := range p.findTranslateVerb() {
		wordCard.AddTranslateVerb(translate)
	}

	for _, translate := range p.findTranslateAdjective() {
		wordCard.AddTranslateAdjective(translate)
	}

	return wordCard, nil
}

func (p *Paragraph77Parser) checkWordPageExist() error {
	selection := p.htmlDoc.Find("h1").First()
	if selection.Length() > 0 {
		return nil
	}
	return errors.New("not found")
}

func (p *Paragraph77Parser) findExample() string {
	selection := p.htmlDoc.Find("#wd_content .block .ex_o").First()
	if selection.Length() > 0 {
		return formatString(selection.Text())
	}
	return ""
}

func (p *Paragraph77Parser) findTranscription() string {
	selection := p.htmlDoc.Find("span.transcription").First()
	if selection.Length() > 0 {
		return formatString(selection.Text())
	}
	return ""
}

func (p *Paragraph77Parser) findAudioDownloadUrl() string {
	selection := p.htmlDoc.Find("audio source").First()
	if selection.Length() > 0 {
		src, found := selection.Attr("src")
		if found {
			return baseUrl + src
		}
	}
	return ""
}

func (p *Paragraph77Parser) findTranslateNoun() []string {
	headerText := []string{"существительное"}
	return p.findTranslate(headerText)
}

func (p *Paragraph77Parser) findTranslateVerb() []string {
	headerText := []string{"глагол"}
	result := p.findTranslate(headerText)

	selection := p.htmlDoc.Find(".light_tr").First()
	if selection.Length() > 0 {
		result = append(result, formatString(selection.Text()))
	}

	return result
}

func (p *Paragraph77Parser) findTranslateAdjective() []string {
	headerText := []string{"прилагательное", "наречие", "предлог"}
	return p.findTranslate(headerText)
}

func (p *Paragraph77Parser) findTranslate(wordTypes []string) []string {
	var result []string

	p.htmlDoc.Find("#wd_content .pos_item").Each(func(i int, s *goquery.Selection) {

		if !isItOneOfNeedsWordTypes(wordTypes, s.Text()) {
			return
		}

		htmlWithTranslate := s.Next()
		htmlWithTranslate.Find(".ex").Each(func(k int, childNode *goquery.Selection) {
			childNode.SetHtml("")
		})

		if htmlWithTranslate.Find("br").Length() > 0 {
			html, err := htmlWithTranslate.Html()
			if err != nil {
				return
			}

			for _, translatePart := range strings.Split(html, "<br/>") {
				doc, err := goquery.NewDocumentFromReader(strings.NewReader(translatePart))
				if err != nil {
					continue
				}

				translate := formatString(doc.Text())

				if len(translate) > 0 {
					result = append(result, formatString(translate))
				}
			}
		} else {
			result = append(result, formatString(s.Next().Text()))
		}
	})

	return result
}

func isItOneOfNeedsWordTypes(needWordTypes []string, foundWordType string) bool {
	for _, headerText := range needWordTypes {
		if strings.Contains(foundWordType, headerText) {
			return true
		}
	}
	return false
}

func formatString(input string) string {
	return strings.TrimSpace(removeStartDash(input))
}

func removeStartDash(input string) string {
	input = strings.Replace(input, "-\u2002", "", 2)
	return input
}

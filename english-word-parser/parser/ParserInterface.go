package parser

import (
	"english-word-parser/entities"
)

type Parser interface {
	Parse(word string) (entities.WordCard, error)
}

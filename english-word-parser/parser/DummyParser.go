package parser

import (
	"english-word-parser/entities"
	"time"
)

type DummyParser struct{}

func NewDummyParser() *DummyParser {
	return &DummyParser{}
}

func (p *DummyParser) Parse(word string) (entities.WordCard, error) {
	wordCard, err := entities.NewWordCard(word)
	time.Sleep(time.Second)
	return wordCard, err
}

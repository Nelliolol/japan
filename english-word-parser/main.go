package main

import (
	"english-word-parser/parser"
	"english-word-parser/queries"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		wordsToParse := c.QueryArray("words[]")
		service := queries.NewParseEnglishWords(func() parser.Parser {
			return parser.NewParagraph77Parser()
		})
		result := service.ParseWords(wordsToParse)
		c.JSON(http.StatusOK, result)
	})
	router.Run()
}

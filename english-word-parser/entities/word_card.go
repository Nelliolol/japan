package entities

import (
	"errors"
	"strings"
)

type WordCard struct {
	Word               string   `json:"word"`
	Transcription      string   `json:"transcription"`
	TranslateNoun      []string `json:"translate_noun"`
	TranslateVerb      []string `json:"translate_verb"`
	TranslateAdjective []string `json:"translate_adjective"`
	Example            string   `json:"example"`
	AudioDownloadUrl   string   `json:"audio_download_url"`
}

func NewWordCard(word string) (WordCard, error) {
	if len(word) < 1 {
		return WordCard{}, errors.New("Can't create wordcard with empty Word")
	}
	return WordCard{Word: word}, nil
}

func (e WordCard) GetWord() string {
	return e.Word
}

func (e *WordCard) GetTranscription() string {
	return e.Transcription
}

func (e *WordCard) SetTranscription(transcription string) {
	e.Transcription = cleanString(transcription)
}

func (e *WordCard) AddTranslateNoun(translate string) {
	e.TranslateNoun = append(e.TranslateNoun, cleanString(translate))
}

func (e WordCard) GetTranslateNoun() []string {
	return e.TranslateNoun
}

func (e *WordCard) AddTranslateVerb(translate string) {
	e.TranslateVerb = append(e.TranslateVerb, cleanString(translate))
}

func (e WordCard) GetTranslateVerb() []string {
	return e.TranslateVerb
}

func (e *WordCard) AddTranslateAdjective(translate string) {
	e.TranslateAdjective = append(e.TranslateAdjective, cleanString(translate))
}

func (e WordCard) GetTranslateAdjective() []string {
	return e.TranslateAdjective
}

func (e *WordCard) GetExample() string {
	return e.Example
}

func (e *WordCard) SetExample(example string) {
	e.Example = cleanString(example)
}

func (e *WordCard) GetAudioDownloadUrl() string {
	return e.AudioDownloadUrl
}

func (e *WordCard) SetAudioDownloadUrl(audioDownloadUrl string) {
	e.AudioDownloadUrl = cleanString(audioDownloadUrl)
}

func cleanString(value string) string {
	return strings.TrimSpace(value)
}

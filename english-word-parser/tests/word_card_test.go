package tests

import (
	"english-word-parser/entities"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetErrorWhenEmptyWord(t *testing.T) {
	_, err := entities.NewWordCard("")
	if err == nil {
		t.Fatalf(`Should be an error`)
	}
}

func TestCreateWordCard(t *testing.T) {
	word := "word"
	transcription := "transcription"
	noun1 := "noun"
	noun2 := "noun2"
	verb1 := "Verb"
	verb2 := "Verb2"
	adjective1 := "Adjective"
	adjective2 := "Adjective2"
	example := "example"
	url := "url"

	wordCard, err := entities.NewWordCard(word)
	wordCard.SetTranscription(transcription)
	wordCard.AddTranslateNoun(noun1)
	wordCard.AddTranslateNoun(noun2)
	wordCard.AddTranslateVerb(verb1)
	wordCard.AddTranslateVerb(verb2)
	wordCard.AddTranslateAdjective(adjective1)
	wordCard.AddTranslateAdjective(adjective2)
	wordCard.SetExample(example)
	wordCard.SetAudioDownloadUrl(url)

	assert.Nil(t, err)
	assert.Equal(t, word, wordCard.GetWord())

	assert.Equal(t, transcription, wordCard.GetTranscription())

	assert.Len(t, wordCard.GetTranslateNoun(), 2)
	assert.Contains(t, wordCard.GetTranslateNoun(), noun1)
	assert.Contains(t, wordCard.GetTranslateNoun(), noun2)

	assert.Len(t, wordCard.GetTranslateVerb(), 2)
	assert.Contains(t, wordCard.GetTranslateVerb(), verb1)
	assert.Contains(t, wordCard.GetTranslateVerb(), verb2)

	assert.Len(t, wordCard.GetTranslateAdjective(), 2)
	assert.Contains(t, wordCard.GetTranslateAdjective(), adjective1)
	assert.Contains(t, wordCard.GetTranslateAdjective(), adjective2)

	assert.Equal(t, example, wordCard.GetExample())
	assert.Equal(t, url, wordCard.GetAudioDownloadUrl())
}

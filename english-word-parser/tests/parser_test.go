package tests

import (
	"english-word-parser/parser"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParser(t *testing.T) {
	word := "word"
	parser := parser.NewDummyParser()
	wordCard, _ := parser.Parse(word)
	assert.Equal(t, word, wordCard.GetWord())
}

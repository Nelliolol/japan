package tests

import (
	"english-word-parser/parser"
	"english-word-parser/queries"
	"github.com/stretchr/testify/assert"
	"testing"
)

func createService() queries.ParseEnglishWords {
	return queries.NewParseEnglishWords(func() parser.Parser {
		return parser.NewDummyParser()
	})
}

func TestParseWordThenSuccess(t *testing.T) {
	word := "word"
	wordsToParse := []string{word}
	service := createService()
	result := service.ParseWords(wordsToParse)
	assert.Len(t, result, 1)
	assert.Equal(t, word, result[0].GetWordCard().GetWord())
}

func TestParseWordThenError(t *testing.T) {
	word := ""
	wordsToParse := []string{word}
	service := createService()
	result := service.ParseWords(wordsToParse)
	assert.Len(t, result, 1)
	assert.NotEmpty(t, result[0].GetError())
}

func TestParseMany(t *testing.T) {
	wordsToParse := []string{"word", "Response", "true"}
	service := createService()
	result := service.ParseWords(wordsToParse)
	assert.Len(t, result, 3)
}

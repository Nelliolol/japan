package tests

import (
	"english-word-parser/entities"
	"english-word-parser/queries"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCreateSuccessParseResponse(t *testing.T) {
	word := "word"

	wordCard, _ := entities.NewWordCard(word)
	parseResponse := queries.NewParseResponse(word)
	parseResponse.SetWordCard(wordCard)

	assert.Equal(t, word, parseResponse.GetWord())
	assert.Equal(t, "", parseResponse.GetError())
	assert.Equal(t, wordCard, parseResponse.GetWordCard())
}

func TestCreateErrorParseResponse(t *testing.T) {
	word := "word"

	errorMessage := "some error"
	parseResponse := queries.NewParseResponse(word)
	parseResponse.SetError(errorMessage)

	assert.Equal(t, word, parseResponse.GetWord())
	assert.Equal(t, errorMessage, parseResponse.GetError())
	assert.Equal(t, entities.WordCard{}, parseResponse.GetWordCard())
}

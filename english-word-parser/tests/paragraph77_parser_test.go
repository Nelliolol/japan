package tests

import (
	"english-word-parser/entities"
	"english-word-parser/parser"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseWhenWordExist(t *testing.T) {
	word := "word"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)

	assert.Equal(t, nil, err)
	assert.Equal(t, word, wordCard.GetWord())
	assert.Equal(t, "|wɜːrd|", wordCard.GetTranscription())
	assert.Equal(t, "Give me your word.", wordCard.GetExample())
	assert.Equal(t, "https://wooordhunt.ru/data/sound/sow/us/word.mp3", wordCard.GetAudioDownloadUrl())
	assert.Len(t, wordCard.GetTranslateVerb(), 1)
	assert.Equal(t, "выражать словами; подбирать слова, выражения; формулировать", wordCard.GetTranslateVerb()[0])
	assert.NotEmpty(t, wordCard.GetTranslateNoun())
	assert.Contains(t, wordCard.GetTranslateNoun(), "слово")
	assert.Empty(t, wordCard.GetTranslateAdjective())
}

func TestParseAdjective(t *testing.T) {
	word := "tired"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)
	assert.Equal(t, nil, err)
	assert.NotEmpty(t, wordCard.GetTranslateAdjective())
	assert.Contains(t, wordCard.GetTranslateAdjective(), "усталый, уставший; утомлённый; уморившийся")
}

func TestParseWithOutAdjectiveTranslate(t *testing.T) {
	word := "envy"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)
	assert.Equal(t, nil, err)
	assert.Equal(t, "envy", wordCard.GetWord())
	assert.Equal(t, "|ˈenvɪ|", wordCard.GetTranscription())
	assert.Equal(t, "зависть", wordCard.GetTranslateNoun()[0])
	assert.Equal(t, "завидовать", wordCard.GetTranslateVerb()[0])
	assert.Empty(t, wordCard.GetTranslateAdjective())
	assert.Equal(t, "They envy us our new house.", wordCard.GetExample())
}

func TestParseWithOutClickableTranslate(t *testing.T) {
	word := "cruel"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)
	assert.Equal(t, nil, err)

	assert.Equal(t, "cruel", wordCard.GetWord())
	assert.Equal(t, "|ˈkruːəl|", wordCard.GetTranscription())
	assert.Equal(t, "жестокий, безжалостный, бессердечный", wordCard.GetTranslateAdjective()[0])
	assert.Equal(t, "мучительный, ужасный, жестокий", wordCard.GetTranslateAdjective()[1])
	assert.Equal(t, "в грам. знач. нареч. эмоц.-усил. очень, чрезвычайно, чертовски", wordCard.GetTranslateAdjective()[2])
	assert.Equal(t, "австрал. разг. портить, грубить, лишать шансов на успех", wordCard.GetTranslateVerb()[0])

	assert.Empty(t, wordCard.GetTranslateNoun())
	assert.NotEmpty(t, wordCard.GetExample())
}

func TestParseWithTwoWordsWhenFound(t *testing.T) {
	word := "get on"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)
	assert.Equal(t, nil, err)

	assert.Equal(t, "get on", wordCard.GetWord())
	assert.Equal(t, "|ˈɡet ɑːn|", wordCard.GetTranscription())
	assert.Equal(t, "надевать", wordCard.GetTranslateVerb()[0])
	assert.Equal(t, "Do your children get on?", wordCard.GetExample())
}

func TestParseWithTwoWordsWhenNotFound(t *testing.T) {
	word := "looking forward"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)
	assert.Equal(t, nil, err)

	assert.Equal(t, "looking forward", wordCard.GetWord())
	assert.Equal(t, "жду с нетерпением", wordCard.GetTranslateVerb()[0])
	assert.Empty(t, wordCard.GetTranscription())
}

func TestParsePredlog(t *testing.T) {
	word := "towards"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)
	assert.Equal(t, nil, err)
	assert.NotEmpty(t, wordCard.GetTranslateAdjective())
}

func TestParseWordWithAppersant(t *testing.T) {
	word := "o'clock"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)
	assert.Equal(t, nil, err)

	assert.Equal(t, "o'clock", wordCard.GetWord())
	assert.Equal(t, "|əˈklɑːk|", wordCard.GetTranscription())
	assert.Equal(t, "по часам, на часах", wordCard.GetTranslateAdjective()[0])
	assert.Equal(t, "It's exactly 3 o'clock.", wordCard.GetExample())
}

func TestExportWithHiddenData(t *testing.T) {
	word := "bare"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)
	assert.Equal(t, nil, err)

	assert.Equal(t, "bare", wordCard.GetWord())
	assert.NotEmpty(t, wordCard.GetTranslateAdjective())
	assert.Len(t, wordCard.GetTranslateVerb(), 4)
}

func TestParseWhenExistSpanAndExistWordWithOutSpan(t *testing.T) {

	word := "betrayal"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)
	assert.Equal(t, nil, err)

	assert.Equal(t, "измена, предательство", wordCard.GetTranslateNoun()[0])
	assert.Equal(t, "признак (чего-л.)", wordCard.GetTranslateNoun()[1])
}

func TestParseWhenWordFromTranslatedAutomatically(t *testing.T) {
	word := "What is up"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)

	assert.Equal(t, nil, err)
	assert.Equal(t, "what is up", wordCard.GetWord())
	assert.Empty(t, wordCard.GetTranscription())
	assert.Equal(t, "Find out what is up. *", wordCard.GetExample())
	assert.Empty(t, wordCard.GetAudioDownloadUrl())
	assert.Len(t, wordCard.GetTranslateVerb(), 1)
	assert.Equal(t, "что случилось", wordCard.GetTranslateVerb()[0])
	assert.Empty(t, wordCard.GetTranslateNoun())
	assert.Empty(t, wordCard.GetTranslateAdjective())
}

func TestParseWhenWordNotExist(t *testing.T) {
	word := "notexistword"
	parser := parser.NewParagraph77Parser()
	wordCard, err := parser.Parse(word)

	assert.NotEmpty(t, err)
	assert.Equal(t, wordCard, entities.WordCard{})
}

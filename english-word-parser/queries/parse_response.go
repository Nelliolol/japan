package queries

import (
	"english-word-parser/entities"
)

type ParseResponse struct {
	Word     string            `json:"word"`
	Error    string            `json:"error"`
	WordCard entities.WordCard `json:"word_card"`
}

func (p *ParseResponse) GetWord() string {
	return p.Word
}

func (p *ParseResponse) GetError() string {
	return p.Error
}

func (p *ParseResponse) SetError(error string) {
	p.Error = error
}

func (p *ParseResponse) GetWordCard() entities.WordCard {
	return p.WordCard
}

func (p *ParseResponse) SetWordCard(wordCard entities.WordCard) {
	p.WordCard = wordCard
}

func NewParseResponse(word string) ParseResponse {
	return ParseResponse{Word: word}
}

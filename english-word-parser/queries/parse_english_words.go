package queries

import (
	"english-word-parser/parser"
	"sort"
	"sync"
)

type ParseEnglishWords struct {
	parser func() parser.Parser
}

func NewParseEnglishWords(parserFabric func() parser.Parser) ParseEnglishWords {
	return ParseEnglishWords{
		parser: parserFabric,
	}
}

func (p ParseEnglishWords) ParseWords(words []string) []ParseResponse {

	var result = []ParseResponse{}
	var wg sync.WaitGroup
	c := make(chan ParseResponse, len(words))
	for _, word := range words {
		wg.Add(1)
		go func(word string, c chan ParseResponse) {
			defer wg.Done()
			response := NewParseResponse(word)
			wordCard, err := p.parser().Parse(word)
			if err != nil {
				response.SetError(err.Error())
			} else {
				response.SetWordCard(wordCard)
			}

			c <- response
		}(word, c)
	}
	wg.Wait()
	close(c)
	for elem := range c {
		result = append(result, elem)
	}

	sort.Slice(result, func(i, j int) bool {
		return findPosition(result[i].Word, words) < findPosition(result[j].Word, words)
	})

	return result
}

func findPosition(word string, words []string) int {
	for index, value := range words {
		if word == value {
			return index
		}
	}
	return -1
}

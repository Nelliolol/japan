package entities

import "errors"

var ErrEmptyFields = errors.New("can't create sentence with empty fields")

type Sentence struct {
	Japanese string `json:"japanese"`
	English  string `json:"english"`
}

func NewSentence(japanese string, english string) (Sentence, error) {
	if len(japanese) < 1 || len(english) < 1 {
		return Sentence{}, ErrEmptyFields
	}

	return Sentence{
		Japanese: japanese,
		English:  english,
	}, nil
}

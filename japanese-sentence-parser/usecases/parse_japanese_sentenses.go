package usecases

import (
	"japanese-sentence-parser/parser"
	"sort"
	"sync"
)

type ParseJapaneseSentences struct {
	parser parser.Parser
}

func NewParseJapaneseSentences(parser parser.Parser) ParseJapaneseSentences {
	return ParseJapaneseSentences{
		parser: parser,
	}
}

func (p *ParseJapaneseSentences) ParseSentences(kanjiToFindSentences []string) []SentenceResponse {

	var result []SentenceResponse
	var wg sync.WaitGroup
	c := make(chan SentenceResponse, len(kanjiToFindSentences))
	for _, kanji := range kanjiToFindSentences {
		wg.Add(1)
		go func(kanji string, c chan SentenceResponse) {
			defer wg.Done()
			response := NewSentenceResponse(kanji)
			sentences, err := p.parser.Parse(kanji)
			if err != nil {
				response.SetError(err.Error())
			} else {
				for _, sentence := range sentences {
					response.AddSentence(sentence)
				}
			}
			c <- response
		}(kanji, c)
	}
	wg.Wait()
	close(c)
	for elem := range c {
		result = append(result, elem)
	}

	sort.Slice(result, func(i, j int) bool {
		return findPosition(result[i].Kanji, kanjiToFindSentences) < findPosition(result[j].Kanji, kanjiToFindSentences)
	})

	return result
}

func findPosition(word string, words []string) int {
	for index, value := range words {
		if word == value {
			return index
		}
	}
	return -1
}

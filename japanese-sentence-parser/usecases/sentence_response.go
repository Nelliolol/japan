package usecases

import "japanese-sentence-parser/entities"

type SentenceResponse struct {
	Kanji     string              `json:"kanji"`
	Error     string              `json:"error"`
	Sentences []entities.Sentence `json:"sentences"`
}

func NewSentenceResponse(kanji string) SentenceResponse {
	return SentenceResponse{Kanji: kanji}
}

func (p *SentenceResponse) GetKanji() string {
	return p.Kanji
}

func (p *SentenceResponse) SetError(error string) {
	p.Error = error
}

func (p *SentenceResponse) GetError() string {
	return p.Error
}

func (p *SentenceResponse) AddSentence(sentence entities.Sentence) {
	p.Sentences = append(p.Sentences, sentence)
}

func (p *SentenceResponse) GetSentences() []entities.Sentence {
	result := make([]entities.Sentence, len(p.Sentences))
	for k, v := range p.Sentences {
		result[k] = v
	}
	return result
}

package parser

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"japanese-sentence-parser/entities"
	"net/http"
	"strings"
)

const baseUrl = "https://jisho.org"

type JishoParser struct{}

type loadingPageError struct {
	statusCode int
	status     string
}

func (e loadingPageError) Error() string {
	return fmt.Sprintf("status code error: %d %s", e.statusCode, e.status)
}

func NewJishoParser() *JishoParser {
	return &JishoParser{}
}

func (p *JishoParser) Parse(kanji string) ([]entities.Sentence, error) {
	doc, err := getHtmlDocument(kanji)
	if err != nil {
		return []entities.Sentence{}, err
	}

	return parseSentences(doc), err
}

func getHtmlDocument(Kanji string) (*goquery.Document, error) {

	res, err := getHtmlContent(Kanji)
	if err != nil {
		return &goquery.Document{}, err
	}

	defer func(res io.ReadCloser) {
		err := res.Close()
		if err != nil {

		}
	}(res)

	doc, err := goquery.NewDocumentFromReader(res)
	if err != nil {
		return &goquery.Document{}, err
	}

	return doc, nil
}

func getHtmlContent(Kanji string) (io.ReadCloser, error) {
	res, err := http.Get(getWordPageUrl(Kanji))
	if err != nil {
		return nil, err
	}

	if res.StatusCode != 200 {
		return nil, loadingPageError{statusCode: res.StatusCode, status: res.Status}
	}

	return res.Body, nil
}

func getWordPageUrl(Kanji string) string {
	return baseUrl + "/search/" + Kanji + "%20%23sentences"
}

func parseSentences(doc *goquery.Document) []entities.Sentence {
	var foundSentences []entities.Sentence
	doc.Find("ul.sentences .sentence_content").Each(func(i int, s *goquery.Selection) {
		sentence, err := createSentence(s)
		if err == nil {
			foundSentences = append(foundSentences, sentence)
		}
	})

	return foundSentences
}

func createSentence(doc *goquery.Selection) (entities.Sentence, error) {
	return entities.NewSentence(
		findJapaneseText(doc),
		findEnglishText(doc))
}

func findEnglishText(doc *goquery.Selection) string {
	return doc.Find(".english").First().Text()
}

func findJapaneseText(doc *goquery.Selection) string {
	var japaneseSentence string
    doc.Find(".furigana").Remove()
    japaneseSentence = doc.Find(".japanese_sentence").Text()
    japaneseSentence = strings.TrimSpace(japaneseSentence)
	return japaneseSentence
}

package parser

import (
	"japanese-sentence-parser/entities"
)

type Parser interface {
	Parse(kanji string) ([]entities.Sentence, error)
}

package parser

import (
	"japanese-sentence-parser/entities"
	"time"
)

type DummyParser struct{}

func NewDummyParser() *DummyParser {
	return &DummyParser{}
}

func (p *DummyParser) Parse(kanji string) ([]entities.Sentence, error) {
	sentence, err := entities.NewSentence(kanji, kanji)
	time.Sleep(time.Second)
	return []entities.Sentence{sentence}, err
}

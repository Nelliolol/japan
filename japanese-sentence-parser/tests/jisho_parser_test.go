package tests

import (
	"github.com/stretchr/testify/assert"
	"japanese-sentence-parser/parser"
	"testing"
)

func TestParseSentenceThenSuccess(t *testing.T) {
	kanji := "食べる"
	jishoParser := parser.NewJishoParser()
	sentences, err := jishoParser.Parse(kanji)

	assert.Nil(t, err)
	assert.True(t, len(sentences) > 1)
    assert.Equal(t, "The most enjoyable part of traveling, after all, is eating the local specialties.", sentences[0].English)
    assert.Equal(t, "旅行の楽しみは、何といってもやはり、その土地の名物料理を食べることだろう。", sentences[0].Japanese)
}

func TestParseSentenceThenNotFound(t *testing.T) {

	kanji := "not exist kanji"
	jishoParser := parser.NewJishoParser()
	sentences, err := jishoParser.Parse(kanji)

	assert.Len(t, sentences, 0)
	assert.Nil(t, err)
}

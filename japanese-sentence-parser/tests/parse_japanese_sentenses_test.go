package tests

import (
	"github.com/stretchr/testify/assert"
	"japanese-sentence-parser/parser"
	"japanese-sentence-parser/usecases"
	"testing"
)

func createService() usecases.ParseJapaneseSentences {
	return usecases.NewParseJapaneseSentences(parser.NewDummyParser())
}

func TestParseKanjiThenSuccess(t *testing.T) {
	kanji := "word"
	kanjiToFindSentences := []string{kanji}
	service := createService()
	result := service.ParseSentences(kanjiToFindSentences)
	assert.Len(t, result, 1)
	assert.Equal(t, kanji, result[0].GetKanji())
}

func TestParseWordThenError(t *testing.T) {
	kanji := ""
	kanjiToFindSentences := []string{kanji}
	service := createService()
	result := service.ParseSentences(kanjiToFindSentences)
	assert.Len(t, result, 1)
	assert.NotEmpty(t, result[0].GetError())
}

func TestParseMany(t *testing.T) {
	kanjiToFindSentences := []string{"word", "Response", "true"}
	service := createService()
	result := service.ParseSentences(kanjiToFindSentences)
	assert.Len(t, result, 3)
}

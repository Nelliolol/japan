package tests

import (
	"github.com/stretchr/testify/assert"
	"japanese-sentence-parser/parser"
	"testing"
)

func TestParser(t *testing.T) {
	kanji := "word"
	sentenceParser := parser.NewDummyParser()
	arSentences, _ := sentenceParser.Parse(kanji)
	assert.NotEmpty(t, kanji, arSentences[0].English)
	assert.NotEmpty(t, kanji, arSentences[0].Japanese)
}

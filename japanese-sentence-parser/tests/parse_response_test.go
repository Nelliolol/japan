package tests

import (
	"github.com/stretchr/testify/assert"
	"japanese-sentence-parser/entities"
	"japanese-sentence-parser/usecases"
	"testing"
)

func TestCreateSuccessParseResponse(t *testing.T) {
	kanji := "word"
	english := "english"
	japanese := "japanese"

	sentence, _ := entities.NewSentence(japanese, english)
	parseResponse := usecases.NewSentenceResponse(kanji)
	parseResponse.AddSentence(sentence)

	assert.Equal(t, kanji, parseResponse.GetKanji())
	assert.Equal(t, "", parseResponse.GetError())
	assert.Equal(t, sentence, parseResponse.GetSentences()[0])
}

func TestCreateErrorParseResponse(t *testing.T) {

	errorMessage := "some error"

	kanji := "word"

	parseResponse := usecases.NewSentenceResponse(kanji)
	parseResponse.Error = errorMessage

	assert.Equal(t, kanji, parseResponse.GetKanji())
	assert.Equal(t, errorMessage, parseResponse.GetError())
	assert.Equal(t, []entities.Sentence{}, parseResponse.GetSentences())
}

package tests

import (
	"github.com/stretchr/testify/assert"
	"japanese-sentence-parser/entities"
	"testing"
)

func TestCreateSentence(t *testing.T) {
	japanese := "japanese"
	english := "english"

	sentence, err := entities.NewSentence(japanese, english)

	assert.Nil(t, err)
	assert.Equal(t, japanese, sentence.Japanese)
	assert.Equal(t, english, sentence.English)
}

func TestCreateSentenceWithError(t *testing.T) {
	japanese := ""
	english := "english"

	sentence, err := entities.NewSentence(japanese, english)

	assert.Equal(t, entities.ErrEmptyFields, err)
	assert.Equal(t, entities.Sentence{}, sentence)
}

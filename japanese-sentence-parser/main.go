package main

import (
	"github.com/gin-gonic/gin"
	"japanese-sentence-parser/parser"
	"japanese-sentence-parser/usecases"
	"net/http"
)

func main() {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		wordsToParse := c.QueryArray("kanji[]")
		service := usecases.NewParseJapaneseSentences(parser.NewJishoParser())
		result := service.ParseSentences(wordsToParse)
		c.JSON(http.StatusOK, result)
	})
	router.Run()
}
